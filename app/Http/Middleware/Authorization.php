<?php

namespace App\Http\Middleware;

use Closure;

class Authorization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->header('x-secretkey') == env('APP_AUTH')) {
            return $next($request);
        }

        return response()->json([
            "metaData" => [
                "code" => "422",
                "message" => "Invalid credentials, Hubungi Administrator ",
            ],
            "response" => null,
        ]);

    }
}
