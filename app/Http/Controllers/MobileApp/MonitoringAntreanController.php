<?php

namespace App\Http\Controllers\MobileApp;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\MobileApp\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MonitoringAntreanController extends Controller
{
    public function getAntreanPoli(Request $request)
    {
        $decode = $request->all();
        $hari = strtoupper(hariindo($this->date));

        if ($hari == 'Minggu') {
            $response = array(
                'metadata' => array(
                    'message' => 'Pastikan Anda tidak memilih hari libur',
                    'code' => 201,
                ),
            );
        } else if ($hari == 'Tidak di ketahui') {
            $response = array(
                'metadata' => array(
                    'message' => 'Hari tidak di ketahui',
                    'code' => 201,
                ),
            );
        } else {
            $polis = DB::table('antrean_polis as a')
                ->join('pasien_polis as b', 'b.id', '=', 'a.pasien_poli_id')
                ->groupBy('b.poli_id', 'b.dokter_id')
                ->whereDate('b.created_at', '=', $this->date)
                ->select('a.id', 'b.poli_id', 'b.dokter_id')
                ->get();

            $data = [];
            foreach ($polis as $key => $poli) {
                $terakhirAntrean = DB::table('antrean_polis as a')
                    ->join('pasien_polis as b', 'b.id', '=', 'a.pasien_poli_id')
                    ->join('polis as c', 'c.id', '=', 'b.poli_id')
                    ->join('pegawais as d', 'd.id', '=', 'b.dokter_id')
                    ->where('b.poli_id', '=', $poli->poli_id)
                    ->where('b.dokter_id', '=', $poli->dokter_id)
                    ->whereDate('b.created_at', '=', $this->date)
                    ->select('a.id', 'a.nomor', 'c.kode as kode_poli', 'c.uraian as uraian_poli', 'd.nama')
                    ->orderBy('a.id', 'DESC')
                    ->first();

                $dataAntrean = DB::table('antrean_polis as a')
                    ->join('pasien_polis as b', 'b.id', '=', 'a.pasien_poli_id')
                    ->join('polis as c', 'c.id', '=', 'b.poli_id')
                    ->join('pegawais as d', 'd.id', '=', 'b.dokter_id')
                    ->where('b.poli_id', '=', $poli->poli_id)
                    ->where('b.dokter_id', '=', $poli->dokter_id)
                    ->whereDate('b.created_at', '=', $this->date)
                    ->select('a.id', 'a.nomor', 'c.kode as kode_poli', 'c.uraian as uraian_poli', 'd.nama')
                    ->where('a.time', '!=', null)
                    ->orderBy('a.id', 'DESC')
                    ->first();

                $totalAntrean = DB::table('antrean_polis as a')
                    ->join('pasien_polis as b', 'b.id', '=', 'a.pasien_poli_id')
                    ->join('polis as c', 'c.id', '=', 'b.poli_id')
                    ->join('pegawais as d', 'd.id', '=', 'b.dokter_id')
                    ->where('b.poli_id', '=', $poli->poli_id)
                    ->where('b.dokter_id', '=', $poli->dokter_id)
                    ->whereDate('b.created_at', '=', $this->date)
                    ->select('a.id', 'a.nomor', 'c.kode as kode_poli', 'c.uraian as uraian_poli', 'd.nama')
                    ->count();

                $sisaAntrean = DB::table('antrean_polis as a')
                    ->join('pasien_polis as b', 'b.id', '=', 'a.pasien_poli_id')
                    ->join('polis as c', 'c.id', '=', 'b.poli_id')
                    ->join('pegawais as d', 'd.id', '=', 'b.dokter_id')
                    ->where('b.poli_id', '=', $poli->poli_id)
                    ->where('b.dokter_id', '=', $poli->dokter_id)
                    ->whereDate('b.created_at', '=', $this->date)
                    ->select('a.id', 'a.nomor', 'c.kode as kode_poli', 'c.uraian as uraian_poli', 'd.nama')
                    ->where('a.time', '=', null)
                    ->count();

                $terakhirAntrean = DB::table('antrean_polis as a')
                    ->join('pasien_polis as b', 'b.id', '=', 'a.pasien_poli_id')
                    ->join('polis as c', 'c.id', '=', 'b.poli_id')
                    ->join('pegawais as d', 'd.id', '=', 'b.dokter_id')
                    ->where('b.poli_id', '=', $poli->poli_id)
                    ->where('b.dokter_id', '=', $poli->dokter_id)
                    ->whereDate('b.created_at', '=', $this->date)
                    ->select('a.id', 'a.nomor', 'c.kode as kode_poli', 'c.uraian as uraian_poli', 'd.nama')
                    ->orderBy('a.id', 'DESC')
                    ->first();

                $antreanSelanjutnya = DB::table('antrean_polis as a')
                    ->join('pasien_polis as b', 'b.id', '=', 'a.pasien_poli_id')
                    ->join('polis as c', 'c.id', '=', 'b.poli_id')
                    ->join('pegawais as d', 'd.id', '=', 'b.dokter_id')
                    ->where('b.poli_id', '=', $poli->poli_id)
                    ->where('b.dokter_id', '=', $poli->dokter_id)
                    ->whereDate('b.created_at', '=', $this->date)
                    ->select('a.id', 'a.nomor', 'c.kode as kode_poli', 'c.uraian as uraian_poli', 'd.nama')
                    ->where('time', '=', null)
                    ->first();

                $data[] = [
                    "id" => isset($dataAntrean->id) ? $dataAntrean->id : $antreanSelanjutnya->id,
                    "dipanggil" => $dataAntrean->nomor ?? '-',
                    "selanjutnya" => $antreanSelanjutnya->nomor ?? '-',
                    "poli" => isset($dataAntrean->uraian_poli) ? $dataAntrean->uraian_poli : $antreanSelanjutnya->uraian_poli,
                    "dokter" => isset($dataAntrean->nama) ? $dataAntrean->nama : $antreanSelanjutnya->nama,
                    "antrean_terakhir" => $terakhirAntrean->nomor ?? 0,
                    "total_antrean" => $totalAntrean,
                    "sisa_antrean" => $sisaAntrean,
                ];

            }
            return ResponseFormatter::success($data);
        }
        return $response;
    }
}
