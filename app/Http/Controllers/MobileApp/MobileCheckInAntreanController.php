<?php

namespace App\Http\Controllers\MobileApp;

use App\Http\Controllers\MobileApp\Controller;
use App\Models\RefMobileJkn;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MobileCheckInAntreanController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->request->add(['waktu' => $this->date_time]);

        $decode = $request->all();
        // return strtotime('2022-11-23 08:17:08') * 1000;
        // return strtotime(date('Y-m-d H:i:s')) * 1000;
        @$tanggal = date("Y-m-d", ($decode['waktu'] / 1000));
        @$tanggalchekcin = date("Y-m-d H:i:s", ($decode['waktu'] / 1000));

        if (empty($decode['kodebooking'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Kode Booking tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['kodebooking'], "'") || strpos($decode['kodebooking'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Kode Booking salah',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } elseif (empty($decode['waktu'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Waktu tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $tanggal)) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Tanggal CheckIn tidak sesuai, format yang benar adalah yyyy-mm-dd',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if ($this->date > $tanggal) {
            $response = array(
                'metadata' => array(
                    'message' => 'Waktu CheckIn tidak berlaku mundur',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else {
            $booking = DB::select("SELECT ref_mobile_jkns.no_booking,ref_mobile_jkns.tgl_periksa,ref_mobile_jkns.status,ref_mobile_jkns.validasi,LEFT(ref_mobile_jkns.pukul_praktek,5) as pukul_praktek from ref_mobile_jkns WHERE ref_mobile_jkns.no_booking='" . validTeks4($decode['kodebooking'], 25) . "'");

            if (empty($booking[0]->status)) {
                $response = array(
                    'metadata' => array(
                        'message' => 'Data Booking tidak ditemukan',
                        'code' => 201,
                    ),
                );
                http_response_code(201);
            } else {
                if ($booking[0]->status == 'Batal') {
                    $response = array(
                        'metadata' => array(
                            'message' => 'Booking Anda Sudah Dibatalkan pada tanggal ' . $booking[0]->validasi,
                            'code' => 201,
                        ),
                    );
                    http_response_code(201);
                } else if ($booking[0]->status == 'CheckIn') {
                    $response = array(
                        'metadata' => array(
                            'message' => 'Anda Sudah CheckIn pada tanggal ' . $booking[0]->validasi,
                            'code' => 201,
                        ),
                    );
                    http_response_code(201);
                } else if ($booking[0]->status == 'Belum') {
                    $interval = DB::select(DB::raw("SELECT TIMESTAMPDIFF(MINUTE,:minute,
                    :tanggalchekcin) AS difference"), [
                        'minute' => $booking[0]->tgl_periksa . ' ' . $booking[0]->pukul_praktek . ':00',
                        'tanggalchekcin' => $tanggalchekcin,
                    ]);

                    if ($interval[0]->difference >= 60) {
                        $response = array(
                            'metadata' => array(
                                'message' => 'Chekin Anda sudah expired. Silahkan konfirmasi ke loket pendaftaran',
                                'code' => 201,
                            ),
                        );
                        http_response_code(201);
                    } else if ($interval[0]->difference <= -60) {
                        $response = array(
                            'metadata' => array(
                                'message' => 'Chekin Anda masih harus menunggu lagi. Silahkan konfirmasi ke loket pendaftaran',
                                'code' => 201,
                            ),
                        );
                        http_response_code(201);
                    } else {
                        $update = RefMobileJkn::where('no_booking', validTeks4($decode['kodebooking'], 25))->update([
                            'status' => 'CheckIn',
                            'validasi' => $this->date_time,
                        ]);

                        if (!empty($update)) {
                            $response = array(
                                'metadata' => array(
                                    'message' => 'Ok',
                                    'code' => 200,
                                ),
                            );
                            http_response_code(200);
                        } else {
                            $response = array(
                                'metadata' => array(
                                    'message' => "Maaf terjadi kesalahan, hubungi Admnistrator..",
                                    'code' => 201,
                                ),
                            );
                            http_response_code(201);
                        }
                    }
                }
            }
        }
        return $response;
    }
}
