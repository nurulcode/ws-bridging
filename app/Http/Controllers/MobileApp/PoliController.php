<?php

namespace App\Http\Controllers\MobileApp;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\MobileApp\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;

class PoliController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPoli()
    {
        $data = DB::table('polis')->where('layanan_id', 2)->get();

        return ResponseFormatter::success($data);
    }

    public function getPoliVclaimOnDate($tgl)
    {
        $hari = strtoupper(hariindo($tgl));

        $poli = DB::table('jadwal_dokters')
            ->join('polis', 'polis.id', '=', 'jadwal_dokters.poli_id')
            ->join('pegawais', 'pegawais.id', '=', 'jadwal_dokters.dokter_id')
            ->join('map_poli_vclaims', 'map_poli_vclaims.poli_id', '=', 'jadwal_dokters.poli_id')
            ->select('polis.id', 'polis.uraian')
            ->where('jadwal_dokters.hari', '=', $hari)
            ->groupBy('jadwal_dokters.poli_id')
            ->get();

        if (count($poli)) {
            return ResponseFormatter::success($poli);
        }

        return ResponseFormatter::error([], 'Poli / Klinik tidak tersedia pada tanggal ' . $tgl);
    }

    public function getPoliOnJadwalDokter(Request $request)
    {
        $poli = DB::table('jadwal_dokters')
            ->join('polis', 'polis.id', '=', 'jadwal_dokters.poli_id')
            ->join('pegawais', 'pegawais.id', '=', 'jadwal_dokters.dokter_id')
            ->select('polis.id', 'polis.uraian')
            ->groupBy('jadwal_dokters.poli_id')
            ->get();

        if (empty($poli)) {
            $response = array(
                'metadata' => array(
                    'message' => 'Poli tidak ditemukan',
                    'code' => 201,
                ),
            );
        } else {
            return ResponseFormatter::success($poli);
        }

        return $response;

    }

}
