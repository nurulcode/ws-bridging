<?php

namespace App\Http\Controllers\MobileApp;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\MobileApp\Controller;
use App\Models\PasienPoli;
use Illuminate\Support\Facades\DB;

class KuotaPoliController extends Controller
{
    public function getSisaKuotaPoli($poli, $tgl)
    {
        $hari = strtoupper(hariindo(validTeks($tgl)));

        if (empty($tgl)) {
            $response = array(
                'metadata' => array(
                    'message' => 'Tanggal Periksa tidak boleh kosong',
                    'code' => 201,
                ),
            );
        } else if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $tgl)) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Tanggal tidak sesuai, format yang benar adalah yyyy-mm-dd',
                    'code' => 201,
                ),
            );
        } else if (empty($poli)) {
            $response = array(
                'metadata' => array(
                    'message' => 'Kode poli tidak boleh kosong.',
                    'code' => 201,
                ),
            );
        } else if ($this->date > $tgl) {
            $response = array(
                'metadata' => array(
                    'message' => 'Tanggal Periksa tidak berlaku mundur / Tanggal Hari ini',
                    'code' => 201,
                ),
            );
        } else if ($hari == 'Minggu') {
            $response = array(
                'metadata' => array(
                    'message' => 'Pastikan Anda tidak memilih hari libur',
                    'code' => 201,
                ),
            );
        } else if ($hari == 'Tidak di ketahui') {
            $response = array(
                'metadata' => array(
                    'message' => 'Hari tidak di ketahui',
                    'code' => 201,
                ),
            );
        } else {
            $kdpoli = DB::table('jadwal_dokters as a')
                ->join('polis as c', 'c.id', '=', 'a.poli_id')
                ->join('pegawais as d', 'd.id', '=', 'a.dokter_id')
                ->where('a.poli_id', '=', validTeks($poli))
                ->where('a.hari', $hari)
                ->select('a.dokter_id', 'd.nama', 'c.kode as kode_poli_rs', 'c.uraian as uraian_poli_rs', 'a.hari', 'a.kuota', 'c.id as poli_id', 'd.id as dokter_id')
                ->get();

            if (!count($kdpoli)) {
                $response = array(
                    'metadata' => array(
                        'message' => 'Poli tidak ditemukan pada hari ' . validTeks($tgl),
                        'code' => 201,
                    ),
                );
            } else {

                $data = [];

                foreach ($kdpoli as $key => $value) {
                    $pasien = PasienPoli::where('poli_id', $value->poli_id)
                        ->where('dokter_id', $value->dokter_id)
                        ->whereDate('tgl_masuk', validTeks($tgl))
                        ->count();

                    $data[] = [
                        "nama" => $value->nama,
                        "kuota" => $value->kuota,
                        "sisa_kuota" => $value->kuota - $pasien,
                        "terdaftar" => $pasien,
                    ];

                }

                return ResponseFormatter::success([
                    'tanggal_periksa' => tanggalIndoDgnHari(validTeks($tgl)),
                    'poli' => $value->uraian_poli_rs,
                    'list' => $data,
                ]);
            }
        }

        return $response;
    }
}
