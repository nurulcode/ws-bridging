<?php

namespace App\Http\Controllers\MobileApp;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\MobileApp\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KetersediaanTempatTidurController extends Controller
{
    public function getTempatTidur(Request $request)
    {
        $tempatTidur = DB::table('ruangs')
            ->join('tempat_tidurs', 'tempat_tidurs.ruang_id', '=', 'ruangs.id')
            ->join('polis', 'polis.id', '=', 'ruangs.poli_id')
            ->join('kelases', 'kelases.id', '=', 'ruangs.kelas_id')
            ->select('ruangs.id', 'ruangs.uraian as ruang', 'tempat_tidurs.jumlah_ruang', 'tempat_tidurs.jumlah as jumlah_kamar', 'tempat_tidurs.terpakai', DB::raw('(tempat_tidurs.jumlah-tempat_tidurs.terpakai) AS kosong'), 'tempat_tidurs.updated_at', 'polis.uraian as ruangan', 'kelases.uraian as kelas')->get();

        return ResponseFormatter::success($tempatTidur);
    }
}
