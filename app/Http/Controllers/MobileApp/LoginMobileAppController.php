<?php

namespace App\Http\Controllers\MobileApp;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\MobileApp\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;

class LoginMobileAppController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //valid credential
        $validator = Validator::make($request->all(), [
            'no_rekam_medis' => 'required',
            'tgl_lahir' => 'required',
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return ResponseFormatter::error(null, 'No RM atau Tgl Lahir Tidak Sesuai.');
        }

        //Crean token
        try {
            $pasien = DB::table('pasiens')
                ->where('no_rekam_medis', '=', $request->no_rekam_medis)
                ->where('tgl_lahir', '=', $request->tgl_lahir)
                ->first();

            if (empty($pasien)) {
                return ResponseFormatter::error(null, 'Data tidak tersedia, silahkan melakukan pendaftaran pada loket.', 202);
            } else {
                $token = auth('api_pasien')->setTTL(3600)->tokenById($pasien->id);
                return $this->createNewToken($token, [
                    'no_rekam_medis' => $pasien->no_rekam_medis,
                    'no_bpjs' => $pasien->no_bpjs,
                    'nama' => $pasien->nama,
                ]);
            }
            //Token created, return with success response and jwt token

        } catch (JWTException $e) {
            return ResponseFormatter::error(null, 'Could not create token.');
        }

    }
}
