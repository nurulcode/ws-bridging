<?php

namespace App\Http\Controllers\MobileApp;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\JknV2\Antrean\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;

class DokterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDokter()
    {
        $data = DB::table('pegawais')->where('sdm', 'Dokter')->get();

        return ResponseFormatter::success($data);
    }

    public function getDokterByPoli($poli, $tgl)
    {
        $hari = strtoupper(hariindo(validTeks($tgl)));

        $jadwal = DB::table('jadwal_dokters as a')
            ->join('pegawais as b', 'b.id', '=', 'a.dokter_id')
            ->join('map_poli_vclaims as c', 'c.poli_id', '=', 'a.poli_id')
            ->join('map_dokter_vclaims as d', 'd.dokter_id', '=', 'a.dokter_id')
            ->where('a.poli_id', '=', $poli)
            ->where('a.hari', '=', $hari)
            ->select([
                'b.id', 'b.nama',
                'a.mulai', 'a.selesai',
                'c.kode_poli_vclaim',
                'd.kode_dokter_vclaim',
            ])
            ->get();

        return ResponseFormatter::success($jadwal);
    }

    public function getDokterPoliTgl($dokter, $poli, $tgl)
    {
        $hari = strtoupper(hariindo(validTeks($tgl)));

        $dokter = DB::table('jadwal_dokters as a')
            ->join('pegawais as b', 'b.id', '=', 'a.dokter_id')
            ->join('map_poli_vclaims as c', 'c.poli_id', '=', 'a.poli_id')
            ->join('map_dokter_vclaims as d', 'd.dokter_id', '=', 'a.dokter_id')
            ->where('c.kode_poli_vclaim', '=', $poli)
            ->where('d.kode_dokter_vclaim', '=', $dokter)
            ->where('a.hari', '=', $hari)
            ->select([
                'b.id', 'b.nama',
                'a.mulai', 'a.selesai',
                'c.kode_poli_vclaim',
                'd.kode_dokter_vclaim',
                'd.uraian_dokter_vclaim',
            ])
            ->first();
        // $jammulai = validTeks2(substr($decode['jampraktek'], 0, 5));

        if (!empty($dokter)) {
            return ResponseFormatter::success([
                'id' => $dokter->id,
                'nama' => $dokter->uraian_dokter_vclaim,
                'jampraktek' => substr($dokter->mulai, 0, 5) . '-' . substr($dokter->selesai, 0, 5),
                'kodepoli' => $dokter->kode_poli_vclaim,
                'kodedokter' => $dokter->kode_dokter_vclaim,
            ]);

        } else {
            return ResponseFormatter::error(null, 'Data dokter tidak ada');
        }
    }

    public function getDokterOnJadwalDokter(Request $request)
    {
        $dokter = DB::table('jadwal_dokters')
            ->join('polis', 'polis.id', '=', 'jadwal_dokters.poli_id')
            ->join('pegawais', 'pegawais.id', '=', 'jadwal_dokters.dokter_id')
            ->select('pegawais.id', 'polis.uraian', 'pegawais.nama')
            ->groupBy('jadwal_dokters.dokter_id')
            ->get();

        if (empty($dokter)) {
            $response = array(
                'metadata' => array(
                    'message' => 'Dokter tidak ditemukan',
                    'code' => 201,
                ),
            );
        } else {
            return ResponseFormatter::success($dokter);
        }

        return $response;

    }
}
