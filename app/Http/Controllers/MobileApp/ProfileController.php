<?php

namespace App\Http\Controllers\MobileApp;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function me()
    {
        return ResponseFormatter::success([
            'no_rekam_medis' => auth('api_pasien')->user()->no_rekam_medis,
            'no_bpjs' => auth('api_pasien')->user()->no_bpjs,
            'nama' => auth('api_pasien')->user()->nama,
        ]);
    }
}
