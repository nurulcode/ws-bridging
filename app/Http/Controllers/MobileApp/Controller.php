<?php

namespace App\Http\Controllers\MobileApp;

use App\Helpers\ResponseFormatter;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    protected $month, $date, $time, $date_time;

    public function __construct()
    {
        date_default_timezone_set('Asia/Jayapura');
        $this->month = date('Y-m');
        $this->date = date('Y-m-d');
        $this->time = date('H:i:s');
        $this->date_time = date('Y-m-d H:i:s');
    }

    protected function createNewToken($token, $pasien)
    {
        return ResponseFormatter::success([
            'pasien' => $pasien,
            'token' => $token,
            'type' => 'bearer',
        ]);
    }

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
