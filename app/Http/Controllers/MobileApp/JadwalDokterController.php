<?php

namespace App\Http\Controllers\MobileApp;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\MobileApp\Controller;
use Illuminate\Support\Facades\DB;

class JadwalDokterController extends Controller
{
    public function getJadwalDokterBaseOnDokter($dokter)
    {
        $jadwalDokter = DB::table('jadwal_dokters as a')
            ->where('dokter_id', $dokter)
            ->select('id', 'hari', 'mulai', 'selesai', 'kuota')
            ->get();

        if (empty($jadwalDokter)) {
            $response = array(
                'metadata' => array(
                    'message' => 'Dokter tidak ditemukan',
                    'code' => 201,
                ),
            );
        } else {
            return ResponseFormatter::success(['list' => $jadwalDokter]);
        }
    }

    public function getJadwalDokterBaseOnJadwalDokter($poli)
    {
        $polis = DB::table('jadwal_dokters')
            ->where('poli_id', '=', $poli)
            ->groupBy('dokter_id')
            ->get();

        $jadwal = [];

        foreach ($polis as $key => $data) {
            $dokter = DB::table('pegawais')
                ->where('id', $data->dokter_id)
                ->get();

            foreach ($dokter as $key => $v) {
                $jadwalDokter = DB::table('jadwal_dokters')
                    ->where('dokter_id', $v->id)
                    ->where('poli_id', $data->poli_id)
                    ->select('id', 'hari', 'mulai', 'selesai', 'kuota', 'dokter_id', 'poli_id')
                    ->get();

                $jadwal[] = [
                    'dokter' => $v->nama,
                    'jadwal' => $jadwalDokter,
                ];
            }
        }

        return ResponseFormatter::success($jadwal);
    }
}
