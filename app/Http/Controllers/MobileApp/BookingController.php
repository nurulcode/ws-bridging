<?php

namespace App\Http\Controllers\MobileApp;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\RefMobileJkn;
use Carbon\Carbon;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getListBooking($norm)
    {
        $tglMulai = Carbon::now()->subDays(30 * 6)->isoFormat('Y-MM-DD');
        $tglAkhir = Carbon::now()->addDays(7)->isoFormat('Y-MM-DD');

        $results = RefMobileJkn::where('no_rekam_medis', $norm)
            ->whereNotNull('kunjungan_id')
            ->whereBetween('tgl_periksa', [$tglMulai, $tglAkhir])
            ->select([
                'no_booking',
                'no_hp',
                'uraian_poli',
                'tgl_periksa',
                'no_referensi',
                'no_antrean',
                'no_rekam_medis',
                'status',
                'validasi',
                'status_kirim',
                'jenis_kunjungan',
            ])
            ->get();

        return ResponseFormatter::success($results);
    }
}
