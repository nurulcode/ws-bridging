<?php

namespace App\Http\Controllers\JknV2\Vclaim;

use Bpjs\Bridging\Vclaim\BridgeVclaim;
use Illuminate\Http\Request;

class PRBController
{
    protected $bridging;

    public function __construct()
    {
        $this->bridging = new BridgeVclaim;
    }

    public function insertPRB(Request $request)
    {
        $endpoint = 'PRB/insert';
        $data = [
            "request" => [
                "t_prb" => [
                    "noSep" => $request->no_sep ?? '',
                    "noKartu" => $request->no_kartu ?? '',
                    "alamat" => $request->alamat ?? '',
                    "email" => $request->email ?? '',
                    "programPRB" => $request->kode_program ?? '',
                    "kodeDPJP" => $request->kode_dpjp ?? '',
                    "keterangan" => $request->keterangan ?? '',
                    "saran" => $request->saran ?? '',
                    "user" => $request->user ?? '',
                    "obat" => $request->obat ?? '',
                ],
            ],
        ];

        $json = json_encode($data);
        // Log::info($json);
        return $this->bridging->postRequest($endpoint, $json);
    }

    public function updatePRB(Request $request)
    {
        $endpoint = 'PRB/Update';
        $data = $request->all();
        return $this->bridging->putRequest($endpoint, $data);
    }

    public function deletePRB(Request $request)
    {
        $endpoint = 'PRB/Delete';
        $data = [
            "request" => [
                "t_prb" => [
                    "noSep" => $request->no_sep ?? '',
                    "noSrb" => $request->no_srb ?? '',
                    "user" => $request->user ?? '',
                ],
            ],
        ];

        $json = json_encode($data);
        // Log::info($json);
        return $this->bridging->deleteResponseNoDecrypt($endpoint, $json);
    }

    public function getNomorSRB($noSurat, $sep)
    {
        $endpoint = 'prb/' . $noSurat . '/nosep/' . $sep;
        return $this->bridging->getRequest($endpoint);
    }

    public function getTanggalSRB($tglMulai, $tglAkhir)
    {
        $endpoint = 'prb/tglMulai/' . $tglMulai . '/tglAkhir/' . $tglAkhir;
        return $this->bridging->getRequest($endpoint);
    }

}
