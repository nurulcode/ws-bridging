<?php

namespace App\Http\Controllers\JknV2\Vclaim;

use Bpjs\Bridging\Vclaim\BridgeVclaim;
use Illuminate\Http\Request;

class RujukanController
{
    protected $bridging;

    public function __construct()
    {
        $this->bridging = new BridgeVclaim;
    }

    public function getRujukanNoRujukanByPCare($noRujukan)
    {
        $endpoint = 'Rujukan/' . $noRujukan;
        return $this->bridging->getRequest($endpoint);
    }

    public function getRujukanNoRujukanByRumahSakit($noRujukan)
    {
        $endpoint = 'Rujukan/RS/' . $noRujukan;
        return $this->bridging->getRequest($endpoint);
    }

    public function getRujukanNoKartuByPCareOne($noKartu)
    {
        $endpoint = 'Rujukan/Peserta/' . $noKartu;
        return $this->bridging->getRequest($endpoint);
    }

    public function getRujukanNoKartuByRumahSakitOne($noKartu)
    {
        $endpoint = 'Rujukan/RS/Peserta/' . $noKartu;
        return $this->bridging->getRequest($endpoint);
    }

    public function getRujukanNoKartuByPCareMulti($noKartu)
    {
        $endpoint = 'Rujukan/List/Peserta/' . $noKartu;
        return $this->bridging->getRequest($endpoint);
    }

    public function getRujukanNoKartuByRumahSakitMulti($noKartu)
    {
        $endpoint = 'Rujukan/RS/List/Peserta/' . $noKartu;
        return $this->bridging->getRequest($endpoint);
    }

    public function postRujukan(Request $request)
    {
        $endpoint = 'Rujukan/2.0/insert';
        $data = [
            "request" => [
                "t_rujukan" => [
                    "noSep" => $request->no_sep ?? "",
                    "tglRujukan" => $request->tgl_rujukan ?? "",
                    "tglRencanaKunjungan" => $request->tgl_rencana ?? "",
                    "ppkDirujuk" => $request->kode_ppk_dirujuk ?? "",
                    "jnsPelayanan" => $request->jenis_pelayanan ?? "",
                    "catatan" => $request->catatan ?? "",
                    "diagRujukan" => $request->kode_diagnosa_rujukan ?? "",
                    "tipeRujukan" => $request->tipe_rujukan ?? "",
                    "poliRujukan" => $request->kode_poli_rujukan ?? "",
                    "user" => $request->user ?? "",
                ],
            ],
        ];

        // return Log::info($data);
        $json = json_encode($data);
        return $this->bridging->postRequest($endpoint, $json);
    }

    public function updateRujukan(Request $request)
    {
        $endpoint = 'Rujukan/2.0/Update';
        $data = [
            "request" => [
                "t_rujukan" => [
                    "noRujukan" => $request->no_rujukan ?? "",
                    "tglRujukan" => $request->tgl_rujukan ?? "",
                    "tglRencanaKunjungan" => $request->tgl_rencana ?? "",
                    "ppkDirujuk" => $request->kode_ppk_dirujuk ?? "",
                    "jnsPelayanan" => $request->jenis_pelayanan ?? "",
                    "catatan" => $request->catatan ?? "",
                    "diagRujukan" => $request->kode_diagnosa_rujukan ?? "",
                    "tipeRujukan" => $request->tipe_rujukan ?? "",
                    "poliRujukan" => $request->kode_poli_rujukan ?? "",
                    "user" => $request->user ?? "",
                ],
            ],
        ];

        // return Log::info($data);
        $json = json_encode($data);
        return $this->bridging->putRequest($endpoint, $json);
    }

    public function deleteRujukan(Request $request)
    {
        $endpoint = 'Rujukan/delete';
        $data = [
            "request" => [
                "t_rujukan" => [
                    "noRujukan" => $request->no_rujukan ?? "",
                    "user" => $request->user ?? "",
                ],
            ],
        ];

        $json = json_encode($data);
        return $this->bridging->deleteResponseNoDecrypt($endpoint, $json);
    }

    public function postRujukanKhusus(Request $request)
    {
        $endpoint = 'Rujukan/Khusus/insert';
        $data = [
            "noRujukan" => $request->no_rujukan ?? '',
            "diagnosa" => $request->diagnosa ?? '',
            "procedure" => $request->prosedur ?? '',
            "user" => $request->user ?? "",
        ];
        $json = json_encode($data);
        return $this->bridging->postRequest($endpoint, $json);
    }

    public function deleteRujukanKhusus(Request $request)
    {
        $endpoint = 'Rujukan/Khusus/delete';
        $data = [
            "request" => [
                "t_rujukan" => [
                    "idRujukan" => $request->id_rujukan,
                    "noRujukan" => $request->no_rujukan,
                    "user" => $request->user,
                ],
            ],
        ];

        // return Log::info($data);
        $json = json_encode($data);
        return $this->bridging->deleteRequest($endpoint, $json);
    }

    public function getListRujukanKhusus($Bulan, $Tahun)
    {
        // 1: Bulan (1,2,3,4,5,6,7,8,9,10,11,12)
        // 2: Tahun (4 digit)
        $endpoint = 'Rujukan/Khusus/List/Bulan/' . $Bulan . '/Tahun/' . $Tahun;
        return $this->bridging->getRequest($endpoint);
    }

    public function getDataListSpesialistik($PPKRujukan, $TglRujukan)
    {
        // Parameter 1: Kode PPK Rujukan : 8 digit
        // Parameter 2: Tanggal rujukan format : yyyy-MM-dd
        $endpoint = 'Rujukan/ListSpesialistik/PPKRujukan/' . $PPKRujukan . '/TglRujukan/' . $TglRujukan;
        return $this->bridging->getRequest($endpoint);
    }

    public function getListSarana($PPKRujukan)
    {
        // Parameter 1: Kode PPK Rujukan : 8 digit
        $endpoint = 'Rujukan/ListSarana/PPKRujukan/' . $PPKRujukan;
        return $this->bridging->getRequest($endpoint);
    }

    public function getListRujukanKeluarRS($tglMulai, $tglAkhir)
    {
        $endpoint = 'Rujukan/Keluar/List/tglMulai/' . $tglMulai . '/tglAkhir/' . $tglAkhir;
        return $this->bridging->getRequest($endpoint);
    }

    public function getRujukanKeluar($noRujukan)
    {
        $endpoint = 'Rujukan/Keluar/' . $noRujukan;
        return $this->bridging->getRequest($endpoint);
    }

    public function getDataJumlahSEPRujukan($jenisRujukan, $noRujukan)
    {
        // Parameter 1: Jenis Rujukan 1 -> fktp, 2 -> fkrtl
        // Parameter 2: No Rujukan
        $endpoint = 'Rujukan/JumlahSEP/' . $jenisRujukan . '/' . $noRujukan;
        return $this->bridging->getRequest($endpoint);
    }
}
