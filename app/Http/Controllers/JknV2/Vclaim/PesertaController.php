<?php

namespace App\Http\Controllers\JknV2\Vclaim;

use Bpjs\Bridging\Vclaim\BridgeVclaim;

class PesertaController
{
    protected $bridging;

    public function __construct()
    {
        $this->bridging = new BridgeVclaim;
    }

    public function getPesertaByNoKartu($noKartu, $tglPalayanan)
    {
        $endpoint = 'Peserta/nokartu/' . $noKartu . '/tglSEP/' . $tglPalayanan;
        return $this->bridging->getRequest($endpoint);
    }

    public function getPesertaByNIK($nik, $tglPalayanan)
    {
        $endpoint = 'Peserta/nik/' . $nik . '/tglSEP/' . $tglPalayanan;
        return $this->bridging->getRequest($endpoint);
    }
}
