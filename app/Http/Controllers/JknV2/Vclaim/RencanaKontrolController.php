<?php

namespace App\Http\Controllers\JknV2\Vclaim;

use Bpjs\Bridging\Vclaim\BridgeVclaim;
use Illuminate\Http\Request;

class RencanaKontrolController
{
    protected $bridging;

    public function __construct()
    {
        $this->bridging = new BridgeVclaim;
    }

    public function insertRencanaKontrol(Request $request)
    {
        $endpoint = 'RencanaKontrol/insert';
        $data = [
            "request" => [
                "noSEP" => $request->no_sep ?? '',
                "kodeDokter" => $request->kode_dokter ?? '',
                "poliKontrol" => $request->kode_poli ?? '',
                "tglRencanaKontrol" => $request->tgl_rencana ?? '',
                "user" => $request->user ?? '',
            ],
        ];
        // return Log::info($data);
        $json = json_encode($data);
        return $this->bridging->postRequest($endpoint, $json);
    }

    public function insertSPRI(Request $request)
    {
        $endpoint = 'RencanaKontrol/InsertSPRI';
        $data = [
            "request" => [
                "noKartu" => $request->no_kartu ?? '',
                "kodeDokter" => $request->kode_dokter ?? '',
                "poliKontrol" => $request->kode_poli ?? '',
                "tglRencanaKontrol" => $request->tgl_rencana ?? '',
                "user" => $request->user ?? '',
            ],
        ];

        $json = json_encode($data);
        return $this->bridging->postRequest($endpoint, $json);
    }

    public function updateRencanaKontrol(Request $request)
    {
        $endpoint = 'RencanaKontrol/Update';
        $data = [
            "request" => [
                "noSuratKontrol" => $request->no_surat_kontrol ?? '',
                "noSEP" => $request->no_sep ?? '',
                "kodeDokter" => $request->kode_dokter ?? '',
                "poliKontrol" => $request->kode_poli ?? '',
                "tglRencanaKontrol" => $request->tgl_rencana ?? '',
                "user" => $request->user ?? '',
            ],
        ];
        // return Log::info($data);
        $json = json_encode($data);
        return $this->bridging->putRequest($endpoint, $json);
    }

    public function updateSPRI(Request $request)
    {
        $endpoint = 'RencanaKontrol/UpdateSPRI';
        $data = [
            "request" => [
                "noSPRI" => $request->no_spri ?? '',
                "kodeDokter" => $request->kode_dokter ?? '',
                "poliKontrol" => $request->kode_poli ?? '',
                "tglRencanaKontrol" => $request->tgl_rencana ?? '',
                "user" => $request->user ?? '',
            ],
        ];
        $json = json_encode($data);
        return $this->bridging->putRequest($endpoint, $json);
    }

    public function deleteRencanaKontrol(Request $request)
    {
        $endpoint = 'RencanaKontrol/Delete';
        $data = [
            "request" => [
                "t_suratkontrol" => [
                    "noSuratKontrol" => $request->no_surat_kontrol ?? '',
                    "user" => $request->user ?? '',
                ],
            ],
        ];
        $json = json_encode($data);
        return $this->bridging->deleteResponseNoDecrypt($endpoint, $json);
    }

    public function getSuratKontrolByNoSurat($noSuratKontrol)
    {
        $endpoint = 'RencanaKontrol/noSuratKontrol/' . $noSuratKontrol;
        return $this->bridging->getRequest($endpoint);
    }

    public function getListRencanaKontrolByNoKartu($bulan, $tahun, $noKartu, $tgl)
    {
        $endpoint = 'RencanaKontrol/ListRencanaKontrol/Bulan/' . $bulan . '/Tahun/' . $tahun . '/Nokartu/' . $noKartu . '/filter/' . $tgl;
        return $this->bridging->getRequest($endpoint);
    }

    public function getListRencanaKontrol($tglAwal, $tglAkhir, $tgl)
    {
        $endpoint = 'RencanaKontrol/ListRencanaKontrol/tglAwal/' . $tglAwal . '/tglAkhir/' . $tglAkhir . '/filter/' . $tgl;
        return $this->bridging->getRequest($endpoint);
    }

    public function getJadwalPraktekDokterRencanaKontrol($JnsKontrol, $KdPoli, $TglRencanaKontrol)
    {
        $endpoint = 'RencanaKontrol/JadwalPraktekDokter/JnsKontrol/' . $JnsKontrol . '/KdPoli/' . $KdPoli . '/TglRencanaKontrol/' . $TglRencanaKontrol;
        return $this->bridging->getRequest($endpoint);
    }

    public function getListSpesialistikRencanaKontrol($JnsKontrol, $nomor, $TglRencanaKontrol)
    {
        $endpoint = 'RencanaKontrol/ListSpesialistik/JnsKontrol/' . $JnsKontrol . '/nomor/' . $nomor . '/TglRencanaKontrol/' . $TglRencanaKontrol;
        return $this->bridging->getRequest($endpoint);
    }

    public function getSearchSEP($noSep)
    {
        $endpoint = 'RencanaKontrol/nosep/' . $noSep;
        return $this->bridging->getRequest($endpoint);
    }
}
