<?php

namespace App\Http\Controllers\JknV2\Vclaim;

use Bpjs\Bridging\Vclaim\BridgeVclaim;
use Illuminate\Http\Request;

class LembarPengajuanKlaimController
{
    protected $bridging;

    public function __construct()
    {
        $this->bridging = new BridgeVclaim;
    }

    public function postLPK(Request $request)
    {
        $endpoint = 'LPK/insert';
        $data = $request->all();
        return $this->bridging->postRequest($endpoint, $data);
    }

    public function updateLPK(Request $request)
    {
        $endpoint = 'LPK/update';
        $data = $request->all();
        return $this->bridging->putRequest($endpoint, $data);
    }

    public function deleteLPK(Request $request)
    {
        $endpoint = 'LPK/delete';
        $data = $request->all();
        return $this->bridging->deleteRequest($endpoint, $data);
    }

    public function getListLPK($TglMasuk, $JnsPelayanan)
    {
        // Parameter 1 : Tanggal Masuk - format : yyyy-MM-dd
        // Parameter 2 : Jenis Pelayanan 1. Inap 2.Jalan
        $endpoint = 'LPK/TglMasuk/' . $TglMasuk . '/JnsPelayanan/' . $JnsPelayanan;
        return $this->bridging->getRequest($endpoint);
    }
}
