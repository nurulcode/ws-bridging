<?php

namespace App\Http\Controllers\JknV2\Vclaim;

use Bpjs\Bridging\Vclaim\BridgeVclaim;
use Illuminate\Http\Request;

class SepController
{
    protected $bridging;

    public function __construct()
    {
        $this->bridging = new BridgeVclaim;
    }

    public function getSep($noSep)
    {
        $endpoint = 'SEP/' . $noSep;
        return $this->bridging->getRequest($endpoint);
    }

    public function postSep(Request $request)
    {
        $endpoint = 'SEP/2.0/insert';
        $data = [
            "request" => [
                "t_sep" => [
                    "noKartu" => $request->no_kartu ?? '',
                    "tglSep" => $request->tgl_sep ?? '',
                    "ppkPelayanan" => $request->kode_ppk_pelayanan ?? '',
                    "jnsPelayanan" => $request->jenis_pelayanan ?? '',
                    "klsRawat" => [
                        "klsRawatHak" => $request->kelas ?? '',
                        "klsRawatNaik" => $request->naik_kelas ?? '',
                        "pembiayaan" => $request->pembiayaan ?? '',
                        "penanggungJawab" => $request->pj_naik_kelas ?? '',
                    ],
                    "noMR" => $request->no_rekam_medis ?? '',
                    "rujukan" => [
                        "asalRujukan" => $request->asal_rujukan ?? '',
                        "tglRujukan" => $request->tgl_rujukan ?? '',
                        "noRujukan" => $request->no_rujukan ?? '',
                        "ppkRujukan" => $request->kode_ppk_rujukan ?? '',
                    ],
                    "catatan" => $request->catatan ?? '',
                    "diagAwal" => $request->kode_diagnosa_awal ?? '',
                    "poli" => [
                        "tujuan" => $request->kode_poli_tujuan ?? '',
                        "eksekutif" => $request->eksekutif ?? '',
                    ],
                    "cob" => [
                        "cob" => $request->cob ?? '',
                    ],
                    "katarak" => [
                        "katarak" => $request->katarak ?? '',
                    ],
                    "jaminan" => [
                        "lakaLantas" => $request->laka_lantas ?? '',
                        "noLP" => $request->no_lp ?? '',
                        "penjamin" => [
                            "tglKejadian" => $request->tgl_kll ?? '',
                            "keterangan" => $request->keterangan_kll ?? '',
                            "suplesi" => [
                                "suplesi" => $request->suplesi ?? '',
                                "noSepSuplesi" => $request->no_suplesi ?? '',
                                "lokasiLaka" => [
                                    "kdPropinsi" => $request->kode_provinsi ?? '',
                                    "kdKabupaten" => $request->kode_kabupaten ?? '',
                                    "kdKecamatan" => $request->kode_kecamatan ?? '',
                                ],
                            ],
                        ],
                    ],
                    "tujuanKunj" => $request->tujuan_kunjungan ?? '',
                    "flagProcedure" => $request->flag_prosedur ?? '',
                    "kdPenunjang" => $request->kode_penunjang ?? '',
                    "assesmentPel" => $request->asesmen_pelayanan ?? '',
                    "skdp" => [
                        "noSurat" => $request->skdp ?? '',
                        "kodeDPJP" => $request->kode_dokter_dpjp ?? '',
                    ],
                    "dpjpLayan" => $request->kode_dpjp_layanan ?? '',
                    "noTelp" => $request->telepon ?? '',
                    "user" => $request->user,
                ],
            ],
        ];

        $json = json_encode($data);
        // Log::info($json);
        return $this->bridging->postRequest($endpoint, $json);
    }

    public function updateSep(Request $request)
    {
        $endpoint = 'SEP/2.0/update';
        $data = [
            "request" => [
                "t_sep" => [
                    "noSep" => $request->no_sep ?? '',
                    "klsRawat" => [
                        "klsRawatHak" => $request->kelas ?? '',
                        "klsRawatNaik" => $request->naik_kelas ?? '',
                        "pembiayaan" => $request->pembiayaan ?? '',
                        "penanggungJawab" => $request->pj_naik_kelas ?? '',
                    ],
                    "noMR" => $request->no_rekam_medis ?? '',
                    "catatan" => $request->catatan ?? '',
                    "diagAwal" => $request->kode_diagnosa_awal ?? '',
                    "poli" => [
                        "tujuan" => $request->kode_poli_tujuan ?? '',
                        "eksekutif" => $request->eksekutif ?? '',
                    ],
                    "cob" => [
                        "cob" => $request->cob ?? '',
                    ],
                    "katarak" => [
                        "katarak" => $request->katarak ?? '',
                    ],
                    "jaminan" => [
                        "lakaLantas" => $request->laka_lantas ?? '',
                        "penjamin" => [
                            "tglKejadian" => $request->tgl_kll ?? '',
                            "keterangan" => $request->keterangan_kll ?? '',
                            "suplesi" => [
                                "suplesi" => $request->suplesi ?? '',
                                "noSepSuplesi" => $request->no_suplesi ?? '',
                                "lokasiLaka" => [
                                    "kdPropinsi" => $request->kode_provinsi ?? '',
                                    "kdKabupaten" => $request->kode_kabupaten ?? '',
                                    "kdKecamatan" => $request->kode_kecamatan ?? '',
                                ],
                            ],
                        ],
                    ],
                    "dpjpLayan" => $request->kode_dpjp_layanan ?? '',
                    "noTelp" => $request->telepon ?? '',
                    "user" => $request->user,
                ],
            ],
        ];

        $json = json_encode($data);
        // return Log::info($json);
        return $this->bridging->putRequest($endpoint, $json);
    }

    public function deleteSep(Request $request)
    {
        $endpoint = 'SEP/2.0/delete';
        $data = [
            "request" => [
                "t_sep" => [
                    "noSep" => $request->no_sep,
                    "user" => $request->user,
                ],
            ],
        ];

        $json = json_encode($data);
        return $this->bridging->deleteResponseNoDecrypt($endpoint, $json);
    }

    public function getSEPJasaRaharja($noKartu, $tglPelayanan)
    {
        $endpoint = 'sep/JasaRaharja/Suplesi/' . $noKartu . '/tglPelayanan/' . $tglPelayanan;
        return $this->bridging->getRequest($endpoint);
    }

    public function getSEPKecelakaanLaluLintas($noKartu)
    {
        $endpoint = 'sep/KllInduk/List/' . $noKartu;
        return $this->bridging->getRequest($endpoint);
    }

    public function postPengajuanSEP(Request $request)
    {
        $endpoint = 'Sep/pengajuanSEP';
        $data = [
            "request" => [
                "t_sep" => [
                    "noKartu" => $request->noKartu ?? '',
                    "tglSep" => $request->tglSep ?? '',
                    "jnsPelayanan" => $request->jnsPelayanan ?? '',
                    "jnsPengajuan" => $request->jnsPengajuan ?? '',
                    "keterangan" => $request->keterangan ?? '',
                    "user" => $request->user ?? '',
                ],
            ],
        ];
        // Log::info($data);

        $json = json_encode($data);
        return $this->bridging->postRequest($endpoint, $json);
    }

    public function postAprovalPengajuanSEP(Request $request)
    {
        $endpoint = 'Sep/aprovalSEP';
        $data = [
            "request" => [
                "t_sep" => [
                    "noKartu" => $request->noKartu ?? '',
                    "tglSep" => $request->tglSep ?? '',
                    "jnsPelayanan" => $request->jnsPelayanan ?? '',
                    "jnsPengajuan" => $request->jnsPengajuan ?? '',
                    "keterangan" => $request->keterangan ?? '',
                    "user" => $request->user ?? '',
                ],
            ],
        ];
        // return Log::info($data);

        $json = json_encode($data);
        return $this->bridging->postRequest($endpoint, $json);
    }

    public function updateTanggalPulang(Request $request)
    {
        $endpoint = 'SEP/2.0/updtglplg';
        $data = [
            "request" => [
                "t_sep" => [
                    "noSep" => $request->no_sep ?? '',
                    "statusPulang" => $request->status_pulang ?? '',
                    "noSuratMeninggal" => $request->no_surat_meninggal ?? '',
                    "tglMeninggal" => $request->tgl_meninggal ?? '',
                    "tglPulang" => $request->tgl_pulang ?? '',
                    "noLPManual" => $request->no_lp_manual ?? '',
                    "user" => $request->user ?? '',
                ],
            ],
        ];

        $json = json_encode($data);
        return $this->bridging->putRequest($endpoint, $json);
    }

    public function getListDataTanggalPulang($bulan, $tahun, $filter = '')
    {
        // Parameter 1: Bulan (1-12)
        // Parameter 2: Tahun
        // Parameter 3: Filter (Apabila dikosongkan akan menampilkan semua data pada bulan dan tahun pilihan)

        $endpoint = 'Sep/updtglplg/list/bulan/' . $bulan . '/tahun/' . $tahun . '/' . $filter;
        return $this->bridging->getRequest($endpoint);
    }

    public function getSEPInternal($noSEP)
    {
        // Log::info($noSEP);
        $endpoint = 'SEP/Internal/' . $noSEP;
        return $this->bridging->getRequest($endpoint);
    }

    public function deleteSEPInternal(Request $request)
    {
        $endpoint = 'SEP/Internal/delete';
        $data = [
            "request" => [
                "t_sep" => [
                    "noSep" => $request->nosep,
                    "noSurat" => $request->nosurat,
                    "tglRujukanInternal" => $request->tglrujukinternal,
                    "kdPoliTuj" => $request->kdpolituj,
                    "user" => $request->user,
                ],
            ],
        ];

        $json = json_encode($data);
        return $this->bridging->deleteResponseNoDecrypt($endpoint, $json);
    }

    public function getFingerPrint($noKartu, $tglPelayanan)
    {
        $endpoint = 'SEP/FingerPrint/Peserta/' . $noKartu . '/TglPelayanan/' . $tglPelayanan;
        return $this->bridging->getRequest($endpoint);
    }

    public function getListFingerPrint($TglPelayanan)
    {
        $endpoint = 'SEP/FingerPrint/List/Peserta/TglPelayanan/' . $TglPelayanan;
        // Log::info($endpoint);
        return $this->bridging->getRequest($endpoint);
    }
}
