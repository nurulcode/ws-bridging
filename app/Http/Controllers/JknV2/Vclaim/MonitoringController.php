<?php

namespace App\Http\Controllers\JknV2\Vclaim;

use Bpjs\Bridging\Vclaim\BridgeVclaim;

class MonitoringController
{
    protected $bridging;

    public function __construct()
    {
        $this->bridging = new BridgeVclaim;
    }

    public function getDataKunjungan($tgl, $jenisPelayanan)
    {
        $endpoint = 'Monitoring/Kunjungan/Tanggal/' . $tgl . '/JnsPelayanan/' . $jenisPelayanan;
        return $this->bridging->getRequest($endpoint);
    }

    public function getDataKlaim($tgl, $jenisPelayanan, $statusKlaim)
    {
        $endpoint = 'Monitoring/Klaim/Tanggal/' . $tgl . '/JnsPelayanan/' . $jenisPelayanan . '/Status/' . $statusKlaim;
        return $this->bridging->getRequest($endpoint);
    }

    public function getHistoriPelayananPeserta($noKartu, $tglMulai, $tglAkhir)
    {
        $endpoint = 'monitoring/HistoriPelayanan/NoKartu/' . $noKartu . '/tglMulai/' . $tglMulai . '/tglAkhir/' . $tglAkhir;
        return $this->bridging->getRequest($endpoint);
    }

    public function getDataKlaimJaminanJasaRaharja($jenisPelayanan, $tglMulai, $tglAkhir)
    {
        $endpoint = 'monitoring/JasaRaharja/JnsPelayanan/' . $jenisPelayanan . '/tglMulai/' . $tglMulai . '/tglAkhir/' . $tglAkhir;
        return $this->bridging->getRequest($endpoint);
    }
}
