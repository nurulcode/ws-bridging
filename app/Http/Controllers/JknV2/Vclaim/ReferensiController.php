<?php

namespace App\Http\Controllers\JknV2\Vclaim;

use Bpjs\Bridging\Vclaim\BridgeVclaim;

class ReferensiController
{
    protected $bridging;

    public function __construct()
    {
        $this->bridging = new BridgeVclaim;
    }

    public function getDiagnosa($uraian)
    {
        $endpoint = 'referensi/diagnosa/' . $uraian;
        return $this->bridging->getRequest($endpoint);
    }

    public function getPoli($uraian)
    {
        $endpoint = 'referensi/poli/' . $uraian;
        return $this->bridging->getRequest($endpoint);
    }

    public function getFaskes($faskes, $jenisFaskes)
    {
        $endpoint = 'referensi/faskes/' . $faskes . '/' . $jenisFaskes;
        return $this->bridging->getRequest($endpoint);
    }

    public function getDokterLayanan($jenis, $tgl, $kode)
    {
        $endpoint = 'referensi/dokter/pelayanan/' . $jenis . '/tglPelayanan/' . $tgl . '/Spesialis/' . $kode;
        return $this->bridging->getRequest($endpoint);
    }

    public function getProvinsi()
    {
        $endpoint = 'referensi/propinsi';
        return $this->bridging->getRequest($endpoint);
    }

    public function getKabupaten($kodeProvinsi)
    {
        $endpoint = 'referensi/kabupaten/propinsi/' . $kodeProvinsi;
        return $this->bridging->getRequest($endpoint);
    }

    public function getKecamatan($kodeKabupaten)
    {
        $endpoint = 'referensi/kecamatan/kabupaten/' . $kodeKabupaten;
        return $this->bridging->getRequest($endpoint);
    }

    public function getDiagnosaPRB()
    {
        $endpoint = 'referensi/diagnosaprb';
        return $this->bridging->getRequest($endpoint);
    }

    public function getObatPRB($namaObat)
    {
        $endpoint = 'referensi/obatprb/' . $namaObat;
        return $this->bridging->getRequest($endpoint);
    }

    public function getProcedure($uraian)
    {
        $endpoint = 'referensi/procedure/' . $uraian;
        return $this->bridging->getRequest($endpoint);
    }

    public function getKelasRawat()
    {
        $endpoint = 'referensi/kelasrawat';
        return $this->bridging->getRequest($endpoint);
    }

    public function getDokter($nama)
    {
        $endpoint = 'referensi/dokter/' . $nama;
        return $this->bridging->getRequest($endpoint);
    }

    public function getSpesialistik()
    {
        $endpoint = 'referensi/spesialistik';
        return $this->bridging->getRequest($endpoint);
    }

    public function getRuangRawat()
    {
        $endpoint = 'referensi/ruangrawat';
        return $this->bridging->getRequest($endpoint);
    }

    public function getCaraKeluar()
    {
        $endpoint = 'referensi/carakeluar';
        return $this->bridging->getRequest($endpoint);
    }

    public function getPascaPulang()
    {
        $endpoint = 'referensi/pascapulang';
        return $this->bridging->getRequest($endpoint);
    }
}
