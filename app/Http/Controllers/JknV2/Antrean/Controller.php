<?php

namespace App\Http\Controllers\JknV2\Antrean;

use App\Helpers\ResponseFormatter;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function __construct()
    {
        date_default_timezone_set('Asia/Jayapura');
        $this->month = date('Y-m');
        $this->date = date('Y-m-d');
        $this->time = date('H:i:s');
        $this->date_time = date('Y-m-d H:i:s');
    }

    protected function createNewToken($token)
    {
        return ResponseFormatter::success([
            'token' => $token,
        ]);
    }

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
