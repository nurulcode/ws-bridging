<?php

namespace App\Http\Controllers\JknV2\Antrean;

use App\Http\Controllers\JknV2\Antrean\Controller;
use App\Models\Kunjungan;
use App\Models\RefMobileJkn;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SisaAntreanController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $decode = $request->all();
        $waktutunggu = 10;
        $response = '';

        if (empty($decode['kodebooking'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Kode Booking tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['kodebooking'], "'") || strpos($decode['kodebooking'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Kode Booking salah',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else {
            // $booking = fetch_array(bukaquery2("select nobooking,no_rawat,tanggalperiksa,status,validasi,nomorreferensi,kodedokter,kodepoli,jampraktek from referensi_mobilejkn_bpjs where nobooking='" . validTeks4($decode['kodebooking'], 25) . "'"));
            $booking = RefMobileJkn::where('no_booking', validTeks4($decode['kodebooking'], 25))->first();

            if (empty($booking->status)) {
                $response = array(
                    'metadata' => array(
                        'message' => 'Data Booking tidak ditemukan',
                        'code' => 201,
                    ),
                );
                http_response_code(201);
            } else if ($this->date > $booking->tgl_periksa) {
                $response = array(
                    'metadata' => array(
                        'message' => 'Antrean tidak ditemukan',
                        'code' => 201,
                    ),
                );
                http_response_code(201);
            } else {
                if ($booking->status == 'Batal') {
                    $response = array(
                        'metadata' => array(
                            'message' => 'Data booking sudah dibatalkan',
                            'code' => 201,
                        ),
                    );
                    http_response_code(201);
                } else if ($booking->status == 'Belum') {
                    $response = array(
                        'metadata' => array(
                            'message' => 'Anda belum melakukan checkin, Silahkan checkin terlebih dahulu',
                            'code' => 201,
                        ),
                    );
                    http_response_code(201);
                } else if ($booking->status == 'CheckIn') {
                    //double poli
                    $jammulai = substr($booking->pukul_praktek, 0, 5);
                    $jamselesai = substr($booking->pukul_praktek, 6, 5);

                    $hari = strtoupper(hariindo($booking->tgl_periksa));

                    $kodedokter = DB::table('map_dokter_vclaims')
                        ->where('kode_dokter_vclaim', '=', $booking->kode_dokter)
                        ->first();

                    $kodepoli = DB::table('map_poli_vclaims as a')
                        ->join('jadwal_dokters as b', 'b.poli_id', '=', 'a.poli_id')
                        ->join('polis as c', 'c.id', '=', 'a.poli_id')
                        ->where('a.kode_poli_vclaim', '=', $booking->kode_poli)
                        ->where('b.dokter_id', $kodedokter->dokter_id)
                        ->where('b.hari', $hari)
                        ->where('b.mulai', $jammulai . ':00')
                        ->where('b.selesai', $jamselesai . ':00')
                        ->select('a.poli_id', 'a.kode_poli_vclaim', 'a.uraian_poli_vclaim', 'c.kode as kode_poli_rs', )
                        ->first();

                    $kunjungan = Kunjungan::where('id', $booking->kunjungan_id)->first();

                    $data = DB::select(DB::raw("SELECT polis.kode as kd_poli, polis.uraian as nm_poli, pegawais.nama as nm_dokter,
                    kunjungans.no_reg, COUNT(kunjungans.no_kunjungan) as total_antrean,
                    IFNULL(SUM(CASE WHEN pasien_polis.kondisi_akhir=12 THEN 1 ELSE 0 END),0) as sisa_antrean
                    FROM kunjungans
                    INNER JOIN pasien_polis ON pasien_polis.kunjungan_id=kunjungans.id
                    INNER JOIN polis ON polis.id=pasien_polis.poli_id
                    INNER JOIN pegawais ON pegawais.id=pasien_polis.dokter_id
                    WHERE pasien_polis.dokter_id='" . $kodedokter->dokter_id . "'
                    AND pasien_polis.poli_id='" . $kodepoli->poli_id . "'
                    AND kunjungans.tgl_kunjungan='" . $booking->tgl_periksa . "'
                    AND CONVERT(RIGHT(kunjungans.no_reg,3),signed)<CONVERT(RIGHT(:noreg,3),signed)
                    "), [
                        'noreg' => $kunjungan->no_reg,
                    ]);

                    $antreanpanggil = DB::select(DB::raw("SELECT kunjungans.no_reg
                    FROM kunjungans
                    INNER JOIN pasien_polis ON pasien_polis.kunjungan_id=kunjungans.id
                    WHERE pasien_polis.kondisi_akhir=12
                    AND pasien_polis.dokter_id='" . $kodedokter->dokter_id . "'
                    AND pasien_polis.poli_id='" . $kodepoli->poli_id . "'
                    AND kunjungans.tgl_kunjungan='" . $booking->tgl_periksa . "'
                    AND CONVERT(RIGHT(kunjungans.no_reg,3),SIGNED)<=CONVERT(RIGHT(:noreg,3),SIGNED)
                    ORDER BY CONVERT(RIGHT(kunjungans.no_reg,3),SIGNED) LIMIT 1
                    "), [
                        'noreg' => $kunjungan->no_reg,
                    ]);

                    if ($data[0]->nm_poli != '') {
                        $response = array(
                            'response' => array(
                                'nomorantrean' => $data[0]->kd_poli . "-" . $kunjungan->no_reg,
                                'namapoli' => $data[0]->nm_poli,
                                'namadokter' => $data[0]->nm_dokter,
                                'sisaantrean' => intval(validangka($data[0]->sisa_antrean) >= 0 ? ($data[0]->sisa_antrean) : 0),
                                'antreanpanggil' => $data[0]->kd_poli . "-" . $antreanpanggil[0]->no_reg,
                                'waktutunggu' => (($data[0]->sisa_antrean * $waktutunggu) * 1000),
                                'keterangan' => "Datanglah Minimal 30 Menit, jika no antrian anda terlewat, silakan konfirmasi ke bagian Pendaftaran atau Perawat Poli, Terima Kasih..",
                            ),
                            'metadata' => array(
                                'message' => 'Ok',
                                'code' => 200,
                            ),
                        );
                        http_response_code(200);
                    } else {
                        $response = array(
                            'metadata' => array(
                                'message' => 'Sisa Antrean Tidak Ditemukan !',
                                'code' => 201,
                            ),
                        );
                        http_response_code(201);
                    }
                } else {
                    $response = array(
                        'metadata' => array(
                            'message' => 'Antrean Tidak Ditemukan !',
                            'code' => 201,
                        ),
                    );
                    http_response_code(201);
                }
            }
        }
        return $response;
    }
}
