<?php

namespace App\Http\Controllers\JknV2\Antrean;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\JknV2\Antrean\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;

class GetTokenController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $data['username'] = $request->headers->get('x-username');
        $data['password'] = $request->headers->get('x-password');

        //valid credential
        $validator = Validator::make($data, [
            'username' => 'required',
            'password' => 'required|string|min:6|max:50',
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return ResponseFormatter::error(null, 'Username atau Password Tidak Sesuai.');
        }

        //Crean token
        try {
            $token = auth('api')->setTTL(50)->attempt($validator->validated());

            if (!$token) {
                return ResponseFormatter::error(null, 'Username atau Password Tidak Sesuai.');
            }

            //Token created, return with success response and jwt token
            return $this->createNewToken($token);

        } catch (JWTException $e) {
            return ResponseFormatter::error(null, 'Could not create token.');
        }

    }
}
