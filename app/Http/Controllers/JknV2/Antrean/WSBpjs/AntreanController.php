<?php

namespace App\Http\Controllers\JknV2\Antrean\WSBpjs;

use App\Http\Controllers\JknV2\Antrean\Controller;
use Bpjs\Bridging\Antrol\BridgeAntrol;
use Illuminate\Http\Request;

class AntreanController extends Controller
{
    protected $bridging;

    public function __construct()
    {
        $this->bridging = new BridgeAntrol;
    }

    public function createAntreanPoli(Request $request)
    {
        $endpoint = 'antrean/add';
        $data = [
            "kodebooking" => $request->no_booking,
            "jenispasien" => $request->jenis_pasien,
            "nomorkartu" => $request->no_kartu,
            "nik" => $request->nik,
            "nohp" => $request->no_hp,
            "kodepoli" => $request->kode_poli,
            "namapoli" => $request->uraian_poli,
            "pasienbaru" => $request->pasien_baru,
            "norm" => $request->no_rekam_medis,
            "tanggalperiksa" => $request->tgl_periksa,
            "kodedokter" => $request->kode_dokter,
            "namadokter" => $request->uraian_dokter,
            "jampraktek" => $request->pukul_praktek,
            "jeniskunjungan" => substr($request->jenis_kunjungan, 0, 1),
            "nomorreferensi" => $request->no_referensi,
            "nomorantrean" => $request->no_antrean,
            "angkaantrean" => $request->angka_antrean,
            "estimasidilayani" => $request->estimasi_dilayani,
            "sisakuotajkn" => $request->sisa_kuota_jkn,
            "kuotajkn" => $request->kuota_jkn,
            "sisakuotanonjkn" => $request->sisa_kuota_non_jkn,
            "kuotanonjkn" => $request->kuota_non_jkn,
            "keterangan" => "Peserta harap 40 menit lebih awal guna pencatatan administrasi.",
        ];

        $json = json_encode($data);
        \Log::info($json);
        return $this->bridging->postRequest($endpoint, $json);
    }

    public function createAntreanFarmasi(Request $request)
    {
        $endpoint = 'antrean/farmasi/add';
        $data = [
            "kodebooking" => "16032021A001",
            "jenisresep" => "racikan / non racikan",
            "nomorantrean" => 1,
            "keterangan" => "",
        ];

        $json = json_encode($data);
        return $this->bridging->postRequest($endpoint, $json);
    }

    public function updateWaktuAntrean(Request $request)
    {
        $endpoint = 'antrean/updatewaktu';
        $data = [
            "kodebooking" => $request->kodebooking,
            "taskid" => $request->taskid,
            "waktu" => $request->waktu,
            "jenisresep" => $request->jenisresep,
        ];

        $json = json_encode($data);
        return $this->bridging->postRequest($endpoint, $json);
    }

    public function batalWaktuAntrean(Request $request)
    {
        $endpoint = 'antrean/batal';
        $data = [
            "kodebooking" => $request->kodebooking,
            "keterangan" => $request->keterangan,
        ];

        $json = json_encode($data);
        // Log::info($json);
        return $this->bridging->postRequest($endpoint, $json);
    }

    public function listWaktuTaskId(Request $request)
    {
        $endpoint = 'antrean/getlisttask';
        $data = [
            "kodebooking" => $request->kodebooking,
        ];

        $json = json_encode($data);
        // Log::info($json);
        return $this->bridging->postRequest($endpoint, $json);
    }
}
