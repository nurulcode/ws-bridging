<?php

namespace App\Http\Controllers\JknV2\Antrean\WSBpjs;

use App\Http\Controllers\JknV2\Antrean\Controller;
use Bpjs\Bridging\Antrol\BridgeAntrol;

class AntreanListController extends Controller
{
    protected $bridging;

    public function __construct()
    {
        $this->bridging = new BridgeAntrol;
    }

    public function listAntreanPerTanggal($tgl)
    {
        $endpoint = 'antrean/pendaftaran/tanggal/' . $tgl;
        return $this->bridging->getRequest($endpoint);
    }

    public function listAntreanPerKode($kodebooking)
    {
        $endpoint = 'antrean/pendaftaran/kodebooking/' . $kodebooking;
        return $this->bridging->getRequest($endpoint);
    }

    public function listAntreanBelumTerlayani()
    {
        $endpoint = 'antrean/pendaftaran/aktif';
        return $this->bridging->getRequest($endpoint);
    }

    public function listAntreanBelumTerlayaniPerPer($poli, $dokter, $hari, $jampraktek)
    {
        $endpoint = 'antrean/pendaftaran/kodepoli/' . $poli . '/kodedokter/' . $dokter . '/hari/' . $hari . '/jampraktek/' . $jampraktek;
        return $this->bridging->getRequest($endpoint);
    }

}
