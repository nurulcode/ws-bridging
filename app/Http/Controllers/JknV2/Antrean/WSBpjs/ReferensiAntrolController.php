<?php

namespace App\Http\Controllers\JknV2\Antrean\WSBpjs;

use App\Http\Controllers\JknV2\Antrean\Controller;
use Bpjs\Bridging\Antrol\BridgeAntrol;

class ReferensiAntrolController extends Controller
{
    protected $bridging;

    public function __construct()
    {
        $this->bridging = new BridgeAntrol;
    }

    public function getPoli()
    {
        $endpoint = 'ref/poli';
        return $this->bridging->getRequest($endpoint);
    }

    public function getDokter()
    {
        $endpoint = 'ref/dokter';
        return $this->bridging->getRequest($endpoint);
    }

    public function getJadwalDokter($poli, $tgl)
    {
        $endpoint = 'jadwaldokter/kodepoli/' . $poli . '/tanggal/' . $tgl;
        $data = $this->bridging->getRequest($endpoint);
        // Log::info($data);
        return $data;
    }

    public function getPoliFp()
    {
        $endpoint = 'ref/poli/fp';
        return $this->bridging->getRequest($endpoint);
    }

    public function getPasienFp($nik, $noka, $noidentitas)
    {
        $endpoint = 'ref/pasien/fp/identitas/' . $nik . '/' . $noka . '/noidentitas/' . $noidentitas;
        return $this->bridging->getRequest($endpoint);
    }
}
