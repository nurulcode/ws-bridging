<?php

namespace App\Http\Controllers\JknV2\Antrean\WSBpjs;

use App\Http\Controllers\JknV2\Antrean\Controller;
use Bpjs\Bridging\Antrol\BridgeAntrol;
use Log;

class DashboardAntreanController extends Controller
{

    protected $bridging;

    public function __construct()
    {
        $this->bridging = new BridgeAntrol;
    }

    public function dasWaktuPerTanggal($tgl, $waktu)
    {
        $endpoint = 'dashboard/waktutunggu/tanggal/' . $tgl . '/waktu/' . $waktu;
        return $this->bridging->getRequest($endpoint);
    }

    public function dasWaktuPerBulan($bulan, $tahun, $waktu)
    {
        $endpoint = 'dashboard/waktutunggu/bulan/' . $bulan . '/tahun/' . $tahun . '/waktu/' . $waktu;
        Log::info($endpoint);
        return $this->bridging->getRequest($endpoint);
    }
}
