<?php

namespace App\Http\Controllers\JknV2\Antrean\WSBpjs;

use App\Http\Controllers\JknV2\Antrean\Controller;
use Bpjs\Bridging\Antrol\BridgeAntrol;
use Illuminate\Http\Request;

class JadwalDokterHfisController extends Controller
{
    protected $bridging;

    public function __construct()
    {
        $this->bridging = new BridgeAntrol;
    }

    public function updateJadwalDokter(Request $request)
    {
        $endpoint = 'jadwaldokter/updatejadwaldokter';
        $data = [
            "kodepoli" => $request->kodepoli ?? '',
            "kodesubspesialis" => $request->kodesubspesialis ?? '',
            "kodedokter" => $request->kodedokter ?? '',
            "jadwal" => $request->jadwal ?? '',
        ];

        $json = json_encode($data);
        return $this->bridging->postRequest($endpoint, $json);
    }

    public function listAntreanPerTanggal($tgl)
    {
        $endpoint = 'antrean/pendaftaran/tanggal/' . $tgl;
        return $this->bridging->getRequest($endpoint);
    }

    public function listAntreanPerKodeBooking($kodebooking)
    {
        $endpoint = 'antrean/pendaftaran/kodebooking/' . $kodebooking;
        return $this->bridging->getRequest($endpoint);
    }

    public function listAntreanBelumTerlayani($kodebooking)
    {
        $endpoint = 'antrean/pendaftaran/aktif';
        return $this->bridging->getRequest($endpoint);
    }
}
