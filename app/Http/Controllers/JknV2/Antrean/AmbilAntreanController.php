<?php

namespace App\Http\Controllers\JknV2\Antrean;

use App\Http\Controllers\JknV2\Antrean\Controller;
use App\Models\Kunjungan;
use App\Models\RefMobileJkn;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class AmbilAntreanController extends Controller
{

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $decode = $request->all();
        $waktutunggu = 10;
        $response = '';

        if (empty($decode['nomorkartu'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Nomor Kartu tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (mb_strlen($decode['nomorkartu'], 'UTF-8') != 13) {
            $response = array(
                'metadata' => array(
                    'message' => 'Nomor Kartu harus 13 digit',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (!preg_match("/^[0-9]{13}$/", $decode['nomorkartu'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Nomor Kartu tidak sesuai',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['nik'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'NIK tidak boleh kosong ',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strlen($decode['nik']) != 16) {
            $response = array(
                'metadata' => array(
                    'message' => 'NIK harus 16 digit ',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (!preg_match("/^[0-9]{16}$/", $decode['nik'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format NIK tidak sesuai',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['nohp'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'No.HP tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['nohp'], "'") || strpos($decode['nohp'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format No.HP salah',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['kodepoli'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Kode Poli tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['kodepoli'], "'") || strpos($decode['kodepoli'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Poli tidak ditemukan',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['kodedokter'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Kode Dokter tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['kodedokter'], "'") || strpos($decode['kodedokter'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Dokter tidak ditemukan',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['tanggalperiksa'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Tanggal tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $decode['tanggalperiksa'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Tanggal tidak sesuai, format yang benar adalah yyyy-mm-dd',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if ($this->date > $decode['tanggalperiksa']) {
            $response = array(
                'metadata' => array(
                    'message' => 'Tanggal Periksa tidak berlaku mundur',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['jampraktek'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Jam Praktek tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['jampraktek'], "'") || strpos($decode['jampraktek'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Jam Praktek tidak ditemukan',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['jeniskunjungan'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Jenis Kunjungan tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['jeniskunjungan'], "'") || strpos($decode['jeniskunjungan'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Jenis Kunjungan tidak ditemukan',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (!preg_match("/^[0-9]{1}$/", $decode['jeniskunjungan'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Jenis Kunjungan tidak sesuai',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (!(($decode['jeniskunjungan'] == "1") || ($decode['jeniskunjungan'] == "2") || ($decode['jeniskunjungan'] == "3") || ($decode['jeniskunjungan'] == "4"))) {
            $response = array(
                'metadata' => array(
                    'message' => 'Jenis Kunjungan tidak ditemukan',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['nomorreferensi'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Nomor Referensi tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['nomorreferensi'], "'") || strpos($decode['nomorreferensi'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Nomor Referensi tidak sesuai format',
                    'code' => 201,
                ),
            );
            http_response_code(201);

        } else if (strlen($decode['nomorreferensi']) < 19) {
            $response = array(
                'metadata' => array(
                    'message' => 'Nomor Referensi tidak sesuai format 19 digit',
                    'code' => 201,
                ),
            );
            http_response_code(201);

        } else if (RefMobileJkn::whereIn('status', ['Belum', 'CheckIn'])->where('no_referensi', validTeks($decode["nomorreferensi"]))->count()) {
            $response = array(
                'metadata' => array(
                    'message' => 'Anda sudah terdaftar dalam antrian menggunakan nomor referensi yang sama',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else {
            $jammulai = validTeks2(substr($decode['jampraktek'], 0, 5));
            $jamselesai = validTeks2(substr($decode['jampraktek'], 6, 5));

            $kddokter = DB::table('map_dokter_vclaims')
                ->where('kode_dokter_vclaim', '=', validTeks($decode['kodedokter']))
                ->first();

            $hari = strtoupper(hariindo(validTeks($decode['tanggalperiksa'])));

            if (empty($kddokter)) {
                $response = array(
                    'metadata' => array(
                        'message' => 'Dokter tidak ditemukan pada hari ' . validTeks($decode['tanggalperiksa']),
                        'code' => 201,
                    ),
                );
                http_response_code(201);

            } else {
                $kdpoli = DB::table('map_poli_vclaims as a')
                    ->join('jadwal_dokters as b', 'b.poli_id', '=', 'a.poli_id')
                    ->join('polis as c', 'c.id', '=', 'a.poli_id')
                    ->where('a.kode_poli_vclaim', '=', validTeks($decode['kodepoli']))
                    ->where('b.dokter_id', $kddokter->dokter_id)
                    ->where('b.hari', $hari)
                    ->where('b.mulai', $jammulai . ':00')
                    ->where('b.selesai', $jamselesai . ':00')
                    ->select('a.poli_id', 'a.kode_poli_vclaim', 'a.uraian_poli_vclaim', 'c.kode as kode_poli_rs', )
                    ->first();
            }

            if (empty($kddokter)) {
                $response = array(
                    'metadata' => array(
                        'message' => 'Dokter tidak ditemukan pada hari ' . validTeks($decode['tanggalperiksa']),
                        'code' => 201,
                    ),
                );
                http_response_code(201);
            } else if (empty($kdpoli)) {
                $response = array(
                    'metadata' => array(
                        'message' => 'Poli tidak ditemukan pada hari ' . validTeks($decode['tanggalperiksa']),
                        'code' => 201,
                    ),
                );
                http_response_code(201);
            } else if (empty($kddokter)) {
                $response = array(
                    'metadata' => array(
                        'message' => 'Dokter tidak ditemukan',
                        'code' => 201,
                    ),
                );
                http_response_code(201);
            } else {
                // sampai
                $jadwal = DB::table('jadwal_dokters as a')
                    ->join('polis as b', 'b.id', '=', 'a.poli_id')
                    ->join('pegawais as c', 'c.id', '=', 'a.dokter_id')
                    ->select('a.*', 'b.uraian as nama_poli', 'c.nama as nama_dokter')
                    ->where('a.dokter_id', '=', $kddokter->dokter_id)
                    ->where('a.poli_id', '=', $kdpoli->poli_id)
                    ->where('a.hari', '=', $hari)
                    ->where('a.mulai', '=', $jammulai . ':00')
                    ->where('a.selesai', '=', $jamselesai . ':00')
                    ->select('a.*', 'b.uraian as nama_poli', 'c.nama as nama_dokter')
                    ->first();

                if (empty($jadwal->kuota)) {
                    $response = array(
                        'metadata' => array(
                            'message' => 'Pendaftaran ke Poli ini tidak tersedia',
                            'code' => 201,
                        ),
                    );
                    http_response_code(201);
                } else {
                    if (empty(cekpasien(validTeks($decode['nik']), validTeks($decode['nomorkartu']), $decode['norm']))) {
                        $response = array(
                            'metadata' => array(
                                'message' => 'Data pasien ini tidak ditemukan',
                                'code' => 202,
                            ),
                        );
                        http_response_code(202);
                    } else {
                        if (strpos($decode['norm'], "'") || strpos($decode['norm'], "\\")) {
                            $response = array(
                                'metadata' => array(
                                    'message' => 'Format No.RM salah',
                                    'code' => 201,
                                ),
                            );
                            http_response_code(201);
                        } else {
                            $sudahdaftar = DB::table('kunjungans as a')
                                ->join('pasiens as b', 'b.id', '=', 'a.pasien_id')
                                ->join('pasien_polis as c', 'c.kunjungan_id', '=', 'a.id')
                                ->where('c.dokter_id', '=', $kddokter->dokter_id)
                                ->where('c.poli_id', '=', $kdpoli->poli_id)
                                ->where('a.tgl_kunjungan', '=', validTeks($decode['tanggalperiksa']))
                                ->where('b.no_bpjs', '=', validTeks($decode['nomorkartu']))
                                ->first();

                            if (!empty($sudahdaftar)) {
                                $response = array(
                                    'metadata' => array(
                                        'message' => "Nomor Antrean hanya dapat diambil 1 kali pada Tanggal, Dokter dan Poli yang sama",
                                        'code' => 201,
                                    ),
                                );
                            } else {
                                $sekarang = $this->date;

                                $interval = DB::SELECT(DB::raw("SELECT TO_DAYS(:tanggalperiksa)-TO_DAYS(:sekarang) as dateInterval"), [
                                    'tanggalperiksa' => validTeks($decode['tanggalperiksa']),
                                    'sekarang' => $sekarang,
                                ]);

                                if ($interval[0]->dateInterval <= 0) {
                                    $response = array(
                                        'metadata' => array(
                                            'message' => 'Pendaftaran ke Poli ini sudah tutup',
                                            'code' => 201,
                                        ),
                                    );
                                    http_response_code(201);
                                } else {
                                    // $sisakuota = getOne2("SELECT count(no_rawat) FROM reg_periksa WHERE kd_poli='$kdpoli' and kd_dokter='$kddokter' and tgl_registrasi='" . validTeks($decode['tanggalperiksa']) . "' ");
                                    $sisakuota = DB::table('pasien_polis')
                                        ->where('poli_id', '=', validTeks($kdpoli->poli_id))
                                        ->where('dokter_id', '=', validTeks($kddokter->dokter_id))
                                        ->where('tgl_masuk', '=', validTeks($decode['tanggalperiksa']))
                                        ->count();

                                    if ($sisakuota < $jadwal->kuota) {

                                        // check data peserta
                                        $datapeserta = cekpasien(validTeks($decode['nik']), validTeks($decode['nomorkartu']), $decode['norm']);
                                        // nomor antrean
                                        $no_reg = noRegPoli($kdpoli->poli_id, $kddokter->dokter_id, validTeks($decode['tanggalperiksa']));
                                        $datamax = DB::select(DB::raw("SELECT IFNULL(MAX(CONVERT(RIGHT(no_kunjungan,5),SIGNED)),0)+1 as datamax FROM kunjungans WHERE tgl_kunjungan='" . validTeks($decode['tanggalperiksa']) . "'"));
                                        $max = $datamax[0]->datamax;
                                        // create no kunjungan
                                        $no_rawat = str_replace("-", "/", $decode['tanggalperiksa'] . "/") . sprintf("%05s", $max);
                                        // kode booking + 1
                                        $datamaxbooking = DB::select(DB::raw(("SELECT IFNULL(MAX(CONVERT(RIGHT(no_booking,5),SIGNED)),0)+1 maxbooking FROM ref_mobile_jkns WHERE tgl_periksa='" . validTeks($decode['tanggalperiksa']))) . "'");
                                        $maxbooking = $datamaxbooking[0]->maxbooking;
                                        // create no booking
                                        $nobooking = str_replace("-", "", validTeks($decode['tanggalperiksa']) . "") . sprintf("%05s", $maxbooking);
                                        // status pasien baru atau lama
                                        $statuspoli = Kunjungan::where('pasien_id', '=', $datapeserta[0]->id)->count() ? 'Lama' : 'Baru';

                                        $dilayani = $no_reg * $waktutunggu;

                                        // status daftar terdaftar di simrs apa blm
                                        $dateC = \Carbon\Carbon::parse($datapeserta[0]->created_at)->isoFormat('YYYY-MM-DD');

                                        $statusdaftar = $dateC == $decode['tanggalperiksa'] ? "1" : "0";

                                        if ($datapeserta[0]->tahun > 0) {
                                            $umur = $datapeserta[0]->tahun;
                                            $sttsumur = "Th";
                                        } else if ($datapeserta[0]->tahun == 0) {
                                            if ($datapeserta[0]->bulan > 0) {
                                                $umur = $datapeserta[0]->bulan;
                                                $sttsumur = "Bl";
                                            } else if ($datapeserta[0]->bulan == 0) {
                                                $umur = $datapeserta[0]->hari;
                                                $sttsumur = "Hr";
                                            }
                                        }

                                        // jenis kunjungan
                                        $jeniskunjungan = "1 (Rujukan FKTP)";
                                        $jenisrujukan = 1;
                                        if ($decode['jeniskunjungan'] == "1") {
                                            $jeniskunjungan = "1 (Rujukan FKTP)";
                                            $jenisrujukan = 2;
                                        } else if ($decode['jeniskunjungan'] == "2") {
                                            $jeniskunjungan = "2 (Rujukan Internal)";
                                            $jenisrujukan = 1;
                                        } else if ($decode['jeniskunjungan'] == "3") {
                                            $jeniskunjungan = "3 (Kontrol)";
                                            $jenisrujukan = 1;
                                        } else if ($decode['jeniskunjungan'] == "4") {
                                            $jeniskunjungan = "4 (Rujukan Antar RS)";
                                            $jenisrujukan = 3;
                                        }

                                        try {
                                            DB::beginTransaction();

                                            $Asuransi = 2;
                                            $RawatJalan = 2;
                                            $AsuransiPemerintah = 4;
                                            $KondisiAkhirBelum = 12;

                                            $kasus = DB::table('kasuses')->where('kode', '-')
                                                ->where('uraian', '-')
                                                ->first();

                                            $kegiatan = DB::table('kegiatans')->where('kode', '-')
                                                ->where('uraian', '-')
                                                ->first();

                                            $res = Http::withHeaders([
                                                'x-secretkey' => env('APP_AUTH'),
                                            ])->post(env('API_SIMRS') . '/api/v1/pendaftaran-rawat-jalan', [
                                                'pasien_id' => $datapeserta[0]->id,
                                                'no_bpjs' => $decode['nomorkartu'],
                                                'tgl_kunjungan' => $decode['tanggalperiksa'],
                                                'pukul_kunjungan' => '080000',
                                                'rujukan_id' => $jenisrujukan, 'pj_nama' => '-',
                                                'pj_telepon' => $decode['nohp'],
                                                'kegiatan_id' => isset($kegiatan->id) ? $kegiatan->id : $kegiatan,
                                                'kasus_id' => isset($kasus->id) ? $kasus->id : $kasus,
                                                'poli_id' => $kdpoli->poli_id,
                                                'jaminan_id' => $Asuransi,
                                                'jenis_registrasi_id' => $AsuransiPemerintah,
                                                'dokter_id' => $kddokter->dokter_id,
                                                'daftar_via' => 'Mobile Jkn',
                                            ]);

                                            $ref = RefMobileJkn::create([
                                                'no_booking' => $nobooking,
                                                'kunjungan_id' => $res['response']['id'],
                                                'pasien_id' => $datapeserta[0]->id,
                                                'no_kartu' => validTeks($decode['nomorkartu']),
                                                'nik' => validTeks($decode['nik']),
                                                'no_hp' => validTeks($decode['nohp']),
                                                'kode_poli' => validTeks($decode['kodepoli']),
                                                'uraian_poli' => $kdpoli->uraian_poli_vclaim,
                                                'pasien_baru' => $statusdaftar,
                                                'no_rekam_medis' => $datapeserta[0]->no_rekam_medis,
                                                'tgl_periksa' => validTeks($decode['tanggalperiksa']),
                                                'kode_dokter' => validTeks($decode['kodedokter']),
                                                'uraian_dokter' => $kddokter->uraian_dokter_vclaim,
                                                'pukul_praktek' => validTeks4($decode['jampraktek'], 20),
                                                'jenis_kunjungan' => validTeks($jeniskunjungan),
                                                'no_referensi' => validTeks($decode['nomorreferensi']),
                                                'no_antrean' => $kdpoli->kode_poli_rs . "-" . $no_reg,
                                                'angka_antrean' => $no_reg,
                                                'estimasi_dilayani' => (strtotime($jadwal->mulai . '+' . $dilayani . ' minute') * 1000),
                                                'kuota_non_jkn' => ($jadwal->kuota - $sisakuota - 1),
                                                'sisa_kuota_non_jkn' => $jadwal->kuota,
                                                'kuota_jkn' => ($jadwal->kuota - $sisakuota - 1),
                                                'sisa_kuota_jkn' => $jadwal->kuota,
                                                'status' => 'Belum',
                                                'status_kirim' => 'Belum',
                                                'daftar_via' => 'Mobile Jkn',
                                            ]);

                                            if (!empty($ref)) {
                                                $response = array(
                                                    'response' => array(
                                                        'nomorantrean' => $kdpoli->kode_poli_rs . "-" . $no_reg,
                                                        'angkaantrean' => intval($no_reg),
                                                        'kodebooking' => $nobooking,
                                                        'pasienbaru' => 0,
                                                        'norm' => $datapeserta[0]->no_rekam_medis,
                                                        'namapoli' => $jadwal->nama_poli,
                                                        'namadokter' => $jadwal->nama_dokter,
                                                        'estimasidilayani' => strtotime($decode['tanggalperiksa'] . " " . $jadwal->mulai . '+' . $dilayani . ' minute') * 1000,
                                                        'sisakuotajkn' => intval($jadwal->kuota - $sisakuota - 1),
                                                        'kuotajkn' => intval($jadwal->kuota),
                                                        'sisakuotanonjkn' => intval($jadwal->kuota - $sisakuota - 1),
                                                        'kuotanonjkn' => intval($jadwal->kuota),
                                                        'keterangan' => 'Peserta harap 40 menit lebih awal guna pencatatan administrasi.',
                                                    ),
                                                    'metadata' => array(
                                                        'message' => 'Ok',
                                                        'code' => 200,
                                                    ),
                                                );

                                                http_response_code(200);
                                            } else {
                                                $response = array(
                                                    'metadata' => array(
                                                        'message' => "Maaf terjadi kesalahan, hubungi Admnistrator..",
                                                        'code' => 201,
                                                    ),
                                                );
                                                http_response_code(201);
                                            }

                                            DB::commit();

                                        } catch (\Exception$e) {
                                            DB::rollback();
                                            return $response = array(
                                                'metadata' => array(
                                                    'message' => 'Maaf Terjadi Kesalahan, Hubungi Admnistrator..',
                                                    'message' => $e->getMessage(),
                                                    'code' => 201,
                                                ),
                                            );
                                            http_response_code(201);
                                        }
                                    } else {
                                        $response = array(
                                            'metadata' => array(
                                                'message' => 'Kuota penuuuh...!',
                                                'code' => 201,
                                            ),
                                        );
                                        http_response_code(201);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return $response;
    }
}
