<?php

namespace App\Http\Controllers\JknV2\Antrean;

use App\Http\Controllers\JknV2\Antrean\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatusAntreanController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $decode = $request->all();

        if (empty($decode['kodepoli'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Kode Poli tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['kodepoli'], "'") || strpos($decode['kodepoli'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Poli tidak ditemukan',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['kodedokter'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Kode Dokter tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['kodedokter'], "'") || strpos($decode['kodedokter'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Dokter tidak ditemukan',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['tanggalperiksa'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Tanggal tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $decode['tanggalperiksa'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Tanggal tidak sesuai, format yang benar adalah yyyy-mm-dd',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if ($this->date > $decode['tanggalperiksa']) {
            $response = array(
                'metadata' => array(
                    'message' => 'Tanggal Periksa tidak berlaku',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['jampraktek'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Jam Praktek tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['jampraktek'], "'") || strpos($decode['jampraktek'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Jam Praktek tidak ditemukan',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else {
            $jammulai = validTeks4(substr($decode['jampraktek'], 0, 5), 20);
            $jamselesai = validTeks4(substr($decode['jampraktek'], 6, 5), 20);

            $hari = strtoupper(hariindo(validTeks4($decode['tanggalperiksa'], 20)));
            $kddokter = DB::table('map_dokter_vclaims')
                ->where('kode_dokter_vclaim', '=', validTeks($decode['kodedokter']))
                ->first();

            $kdpoli = DB::table('map_poli_vclaims as a')
                ->join('jadwal_dokters as b', 'b.poli_id', '=', 'a.poli_id')
                ->join('polis as c', 'c.id', '=', 'a.poli_id')
                ->where('a.kode_poli_vclaim', '=', validTeks($decode['kodepoli']))
                ->where('b.dokter_id', $kddokter->dokter_id)
                ->where('b.hari', $hari)
                ->where('b.mulai', $jammulai . ':00')
                ->where('b.selesai', $jamselesai . ':00')
                ->select('a.poli_id', 'a.kode_poli_vclaim', 'a.uraian_poli_vclaim', 'c.kode as kode_poli_rs', )
                ->first();

            if (empty($kdpoli)) {
                $response = array(
                    'metadata' => array(
                        'message' => 'Poli tidak ditemukan',
                        'code' => 201,
                    ),
                );
                http_response_code(201);
            } else if (empty($kddokter)) {
                $response = array(
                    'metadata' => array(
                        'message' => 'Dokter tidak ditemukan',
                        'code' => 201,
                    ),
                );
                http_response_code(201);
            } else {
                $kuota = DB::table('jadwal_dokters as a')
                    ->join('polis as b', 'b.id', '=', 'a.poli_id')
                    ->join('pegawais as c', 'c.id', '=', 'a.dokter_id')
                    ->select('a.*', 'b.uraian as nama_poli', 'c.nama as nama_dokter')
                    ->where('a.dokter_id', '=', $kddokter->dokter_id)
                    ->where('a.poli_id', '=', $kdpoli->poli_id)
                    ->where('a.hari', '=', $hari)
                    ->where('a.mulai', '=', $jammulai . ':00')
                    ->where('a.selesai', '=', $jamselesai . ':00')
                    ->select('a.*', 'b.uraian as nama_poli', 'c.nama as nama_dokter')
                    ->first();

                if (empty($kuota->kuota)) {
                    $response = array(
                        'metadata' => array(
                            'message' => 'Pendaftaran ke Poli ini tidak tersedia',
                            'code' => 201,
                        ),
                    );
                    http_response_code(201);
                } else {
                    // $data = fetch_array(bukaquery2("SELECT poliklinik.nm_poli,COUNT(reg_periksa.kd_poli) as total_antrean,dokter.nm_dokter,
                    //                 IFNULL(SUM(CASE WHEN reg_periksa.stts ='Belum' THEN 1 ELSE 0 END),0) as sisa_antrean,
                    //                 ('Datanglah Minimal 30 Menit, jika no antrian anda terlewat, silakan konfirmasi ke bagian Pendaftaran atau Perawat Poli, Terima Kasih ..') as keterangan
                    //                 FROM reg_periksa INNER JOIN poliklinik ON poliklinik.kd_poli=reg_periksa.kd_poli INNER JOIN dokter ON reg_periksa.kd_dokter=dokter.kd_dokter
                    //                 WHERE reg_periksa.tgl_registrasi='" . validTeks4($decode['tanggalperiksa'], 20) . "' AND reg_periksa.kd_poli='$kdpoli' and reg_periksa.kd_dokter='$kddokter'"));

                    $data = DB::select("SELECT polis.uraian AS nm_poli, COUNT(pasien_polis.poli_id) AS total_antrean,pegawais.nama AS nm_dokter,
                                    IFNULL(SUM(CASE WHEN pasien_polis.kondisi_akhir ='12' THEN 1 ELSE 0 END),0) AS sisa_antrean,
                                    ('Datanglah Minimal 30 Menit, jika no antrian anda terlewat, silakan konfirmasi ke bagian Pendaftaran atau Perawat Poli, Terima Kasih ..') as keterangan
                                    FROM pasien_polis INNER JOIN polis ON polis.id=pasien_polis.poli_id
                                    INNER JOIN pegawais ON pasien_polis.dokter_id=pegawais.id
                                    WHERE pasien_polis.tgl_masuk='" . validTeks4($decode['tanggalperiksa'], 20) . "'
                                    AND pasien_polis.poli_id='" . $kdpoli->poli_id . "'
                                    AND pasien_polis.dokter_id='" . $kddokter->dokter_id . "'");

                    if ($data[0]->sisa_antrean > 0) {
                        // $no_reg = getOne2("select reg_periksa.no_reg from reg_periksa where reg_periksa.stts='Belum' and reg_periksa.kd_dokter='$kddokter' and reg_periksa.kd_poli='$kdpoli' and reg_periksa.tgl_registrasi='" . validTeks4($decode['tanggalperiksa'], 20) . "' order by CONVERT(RIGHT(reg_periksa.no_reg,3),signed) limit 1 ");
                        $antreanpanggil = DB::select("SELECT kunjungans.no_reg as antreanpanggil
                        FROM kunjungans
                        INNER JOIN pasien_polis ON pasien_polis.kunjungan_id=kunjungans.id
                        WHERE pasien_polis.kondisi_akhir=12
                        AND pasien_polis.dokter_id='" . $kddokter->dokter_id . "'
                        AND pasien_polis.poli_id='" . $kdpoli->poli_id . "'
                        AND kunjungans.tgl_kunjungan='" . validTeks4($decode['tanggalperiksa'], 20) . "'
                        ORDER BY CONVERT(RIGHT(kunjungans.no_reg,3),SIGNED) LIMIT 1");

                        $response = array(
                            'response' => array(
                                'namapoli' => $data[0]->nm_poli,
                                'namadokter' => $data[0]->nm_dokter,
                                'totalantrean' => intval($data[0]->total_antrean),
                                'sisaantrean' => intval(validangka($data[0]->sisa_antrean) >= 0 ? ($data[0]->sisa_antrean) : 0),
                                'antreanpanggil' => $kdpoli->kode_poli_rs . "-" . $antreanpanggil[0]->antreanpanggil,
                                'sisakuotajkn' => intval($kuota->kuota - $data[0]->total_antrean),
                                'kuotajkn' => intval($kuota->kuota),
                                'sisakuotanonjkn' => intval($kuota->kuota - $data[0]->total_antrean),
                                'kuotanonjkn' => intval($kuota->kuota),
                                'keterangan' => $data[0]->keterangan,
                            ),
                            'metadata' => array(
                                'message' => 'Ok',
                                'code' => 200,
                            ),
                        );
                        http_response_code(200);
                    } else {
                        $response = array(
                            'metadata' => array(
                                'message' => 'Maaf belum ada antrian ditanggal ' . FormatTgl(("d-m-Y"), $decode['tanggalperiksa']),
                                'code' => 201,
                            ),
                        );
                        http_response_code(201);
                    }
                }
            }
        }
        return $response;
    }
}
