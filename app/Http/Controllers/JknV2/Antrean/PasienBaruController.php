<?php

namespace App\Http\Controllers\JknV2\Antrean;

use App\Http\Controllers\JknV2\Antrean\Controller;
use App\Models\Pasien;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PasienBaruController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

        $decode = $request->all();
        if (empty($decode['nomorkartu'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Nomor Kartu tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (mb_strlen($decode['nomorkartu'], 'UTF-8') != 13) {
            $response = array(
                'metadata' => array(
                    'message' => 'Nomor Kartu harus 13 digit',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (!preg_match("/^[0-9]{13}$/", $decode['nomorkartu'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Nomor Kartu tidak sesuai',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } elseif (empty($decode['nik'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'NIK tidak boleh kosong ',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } elseif (strlen($decode['nik']) != 16) {
            $response = array(
                'metadata' => array(
                    'message' => 'NIK harus 16 digit ',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (!preg_match("/^[0-9]{16}$/", $decode['nik'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format NIK tidak sesuai',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } elseif (empty($decode['nomorkk'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Nomor KK tidak boleh kosong ',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } elseif (strlen($decode['nomorkk']) != 16) {
            $response = array(
                'metadata' => array(
                    'message' => 'Nomor KK harus 16 digit ',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (!preg_match("/^[0-9]{16}$/", $decode['nomorkk'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Nomor KK tidak sesuai',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['nama'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Nama tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['nama'], "'") || strpos($decode['nama'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Nama salah',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['jeniskelamin'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Jenis Kelamin tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['jeniskelamin'], "'") || strpos($decode['jeniskelamin'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Jenis Kelamin tidak ditemukan',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (!(($decode['jeniskelamin'] == "L") || ($decode['jeniskelamin'] == "P"))) {
            $response = array(
                'metadata' => array(
                    'message' => 'Jenis Kelmain tidak ditemukan',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['tanggallahir'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Tanggal Lahir tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $decode['tanggallahir'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Tanggal Lahir tidak sesuai, format yang benar adalah yyyy-mm-dd',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if ($decode['tanggallahir'] > $this->date) {
            $response = array(
                'metadata' => array(
                    'message' => 'Tanggal lahir pasien salah',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['nohp'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'No.HP tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['nohp'], "'") || strpos($decode['nohp'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format No.HP salah',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['alamat'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Alamat tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['alamat'], "'") || strpos($decode['alamat'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Alamat salah',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['kodeprop'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Kode Propinsi tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['kodeprop'], "'") || strpos($decode['kodeprop'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Kode Propinsi salah',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['namaprop'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Nama Propinsi tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['namaprop'], "'") || strpos($decode['namaprop'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Nama Propinsi salah',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['kodedati2'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Kode Dati II tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['kodedati2'], "'") || strpos($decode['kodedati2'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Kode Dati II salah',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['namadati2'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Nama Dati II tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['namadati2'], "'") || strpos($decode['namadati2'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Nama Dati II salah',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['kodekec'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Kode Kecamatan tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['kodekec'], "'") || strpos($decode['kodekec'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Kode Kecamatan salah',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['namakec'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Nama Kecamatan tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['namakec'], "'") || strpos($decode['namakec'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Nama Kecamatan salah',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['kodekel'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Kode Kelurahan tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['kodekel'], "'") || strpos($decode['kodekel'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Kode Kelurahan salah',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['namakel'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Nama Kelurahan tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['namakel'], "'") || strpos($decode['namakel'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Nama Kelurahan salah',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['rw'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'RW tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['rw'], "'") || strpos($decode['rw'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format RW salah',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['rt'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'RT tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['rt'], "'") || strpos($decode['rt'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format RT salah',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else {
            if (!empty(cekpasienbaru($decode['nik'], $decode['nomorkartu']))) {
                $response = array(
                    'metadata' => array(
                        'message' => 'Pasien dengan NIK dan No.Kartu tersebut sudah terdaftar',
                        'code' => 201,
                    ),
                );
                http_response_code(201);
            } else {
                $setrm = DB::table('pasiens')->orderby('no_rekam_medis', 'desc')->first();

                if (isset($setrm->no_rekam_medis)) {
                    $RM = $setrm->no_rekam_medis;
                } else {
                    $RM = 00000;
                }

                $rekam_medis = str_pad($RM + 1, 6, 0, STR_PAD_LEFT);

                try {
                    DB::beginTransaction();

                    $pasien = new Pasien();
                    $pasien->no_rekam_medis = $rekam_medis;
                    $pasien->waktu_registrasi = $this->date_time;
                    $pasien->no_nik = validTeks4($decode['nik'], 25);
                    $pasien->no_bpjs = validTeks4($decode['nomorkartu'], 20);
                    $pasien->nama = Str::upper(validTeks4($decode['nama'], 60));
                    $pasien->tgl_lahir = validTeks4($decode['tanggallahir'], 30);
                    $pasien->jenis_kelamin = validTeks4($decode['jeniskelamin'], 5);
                    $pasien->telepon = validTeks4($decode['nohp'], 14);
                    $pasien->jenis_identitas = 'Kartu Keluarga';
                    $pasien->no_identitas = validTeks4($decode['nomorkk'], 30);
                    $pasien->nama_ibu = '-';
                    $pasien->tempat_lahir = '-';

                    $pasien->alamat = Str::upper(validTeks4($decode['alamat'], 200) . ' Kelurahan ' . validTeks4($decode['namakel'], 100) . ' kecamatan ' . validTeks4($decode['namakec'], 100) . ' Kabupaten ' . validTeks4($decode['namadati2'], 100) . ' Provinsi ' . validTeks4($decode['namaprop'], 100) . ' RT ' . validTeks4($decode['rt'], 100) . ' RW ' . validTeks4($decode['rw'], 100));
                    $pasien->save();

                    DB::commit();
                } catch (\Exception$e) {
                    DB::rollback();
                    $response = array(
                        'metadata' => array(
                            // 'message' => $e->getMessage(),
                            'message' => 'Maaf Terjadi Kesalahan, Hubungi Admnistrator..',
                            'code' => 201,
                        ),
                    );
                    http_response_code(201);
                }

                if (!empty($pasien)) {
                    $response = array(
                        'response' => array(
                            'norm' => $pasien->no_rekam_medis,
                        ),
                        'metadata' => array(
                            'message' => 'Pasien berhasil mendapatkann nomor RM, silahkan lanjutkan ke booking. Harap datang ke admisi untuk melengkapi data rekam medis',
                            'code' => 200,
                        ),
                    );
                    http_response_code(200);
                } else {
                    $response = array(
                        'metadata' => array(
                            'message' => 'Maaf Terjadi Kesalahan, Hubungi Admnistrator..',
                            'code' => 201,
                        ),
                    );
                    http_response_code(201);
                }
            }
        }
        return $response;
    }
}
