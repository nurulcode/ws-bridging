<?php

namespace App\Http\Controllers\JknV2\Antrean;

use App\Http\Controllers\JknV2\Antrean\Controller;
use App\Models\RefMobileJkn;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CheckInAntreanController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

        $decode = $request->all();
        // return strtotime('2022-11-23 08:17:08') * 1000;
        // return strtotime(date('Y-m-d H:i:s')) * 1000;
        @$tanggal = date("Y-m-d", ($decode['waktu'] / 1000));
        @$tanggalchekcin = date("Y-m-d H:i:s", ($decode['waktu'] / 1000));

        if (empty($decode['kodebooking'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Kode Booking tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['kodebooking'], "'") || strpos($decode['kodebooking'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Kode Booking salah',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } elseif (empty($decode['waktu'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Waktu tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $tanggal)) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Tanggal CheckIn tidak sesuai, format yang benar adalah yyyy-mm-dd',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if ($this->date > $tanggal) {
            $response = array(
                'metadata' => array(
                    'message' => 'Waktu CheckIn tidak berlaku mundur',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else {
            $booking = DB::table('ref_mobile_jkns')
                ->where('no_booking', '=', validTeks4($decode['kodebooking'], 25))
                ->select(['kode_poli', 'kode_dokter', 'kunjungan_id', 'no_booking', 'tgl_periksa', 'status', 'validasi', DB::raw('LEFT(pukul_praktek,5) as pukul_praktek')])
                ->first();

            // $hari = strtoupper(hariindo(validTeks($booking->tgl_periksa)));

            $dokterAktif = DB::table('pegawais as a')
                ->join('jadwal_dokters as b', 'b.dokter_id', '=', 'a.id')
                ->join('map_dokter_vclaims as c', 'c.dokter_id', '=', 'b.dokter_id')
                ->join('map_poli_vclaims as d', 'd.poli_id', '=', 'b.poli_id')
                ->where('a.status', '=', 'Aktif')
                ->where('c.kode_dokter_vclaim', '=', $booking->kode_dokter)
                ->where('d.kode_poli_vclaim', '=', $booking->kode_poli)
                ->select('a.id', 'a.nama')
                ->first();

            if (empty($dokterAktif)) {
                $response = array(
                    'metadata' => array(
                        'message' => 'Gagal CheckIn, ada perubahan jadwal / dokter praktek tidak bisa hadir. Demi kenyamanan silahkan melakukan pendaftaran ualang dengan tanggal yang berbeda',
                        'code' => 201,
                    ),
                );
                http_response_code(201);
            } else if (empty($booking->status)) {
                $response = array(
                    'metadata' => array(
                        'message' => 'Data Booking tidak ditemukan',
                        'code' => 201,
                    ),
                );
                http_response_code(201);
            } else {
                if ($booking->status == 'Batal') {
                    $response = array(
                        'metadata' => array(
                            'message' => 'Booking Anda Sudah Dibatalkan pada tanggal ' . $booking->validasi,
                            'code' => 201,
                        ),
                    );
                    http_response_code(201);
                } else if ($booking->status == 'CheckIn') {
                    $response = array(
                        'metadata' => array(
                            'message' => 'Anda Sudah CheckIn pada tanggal ' . $booking->validasi,
                            'code' => 201,
                        ),
                    );
                    http_response_code(201);
                } else if ($booking->status == 'Belum') {
                    $interval = DB::select(DB::raw("SELECT TIMESTAMPDIFF(MINUTE,:menit,
                    :tanggalchekcin) AS difference"), [
                        'menit' => $booking->tgl_periksa . ' ' . $booking->pukul_praktek . ':00',
                        'tanggalchekcin' => $tanggalchekcin,
                    ]);

                    if ($interval[0]->difference >= 60) {
                        $response = array(
                            'metadata' => array(
                                'message' => 'Chekin Anda sudah expired. Silahkan konfirmasi ke loket pendaftaran',
                                'code' => 201,
                            ),
                        );
                        http_response_code(201);
                    } else if ($interval[0]->difference <= -60) {
                        $response = array(
                            'metadata' => array(
                                'message' => 'CheckIn pada ' . $booking->tgl_periksa . ' dan pukul ' . $booking->pukul_praktek . ' . Silahkan konfirmasi ke loket pendaftaran',
                                'code' => 201,
                            ),
                        );
                        http_response_code(201);
                    } else {
                        $update = RefMobileJkn::where('no_booking', validTeks4($decode['kodebooking'], 25))->update([
                            'status' => 'CheckIn',
                            'validasi' => $this->date_time,
                        ]);

                        if (!empty($update)) {
                            $response = array(
                                'metadata' => array(
                                    'message' => 'Ok',
                                    'code' => 200,
                                ),
                            );
                            http_response_code(200);
                        } else {
                            $response = array(
                                'metadata' => array(
                                    'message' => "Maaf terjadi kesalahan, hubungi Admnistrator..",
                                    'code' => 201,
                                ),
                            );
                            http_response_code(201);
                        }
                    }
                }
            }
        }
        return $response;
    }
}
