<?php

namespace App\Http\Controllers\JknV2\Antrean;

use App\Http\Controllers\JknV2\Antrean\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JadwalOperasiPasienController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $decode = $request->all();

        if (empty($decode['nopeserta'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Nomor Peserta tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (mb_strlen($decode['nopeserta'], 'UTF-8') != 13) {
            $response = array(
                'metadata' => array(
                    'message' => 'Nomor Peserta harus 13 digit',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (!preg_match("/^[0-9]{13}$/", $decode['nopeserta'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Nomor Peserta tidak sesuai',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else {

            // if (count($queryoperasipasien) > 0) {

            $queryoperasipasiens = DB::table('booking_operasis as a')
                ->join('kunjungans as b', 'b.id', '=', 'a.kunjungan_id')
                ->join('pasien_polis as c', 'c.kunjungan_id', '=', 'b.id')
                ->join('polis as d', 'd.id', '=', 'c.poli_id')
                ->join('pasiens as e', 'e.id', '=', 'b.pasien_id')
                ->where('e.no_bpjs', '=', validTeks4($decode['nopeserta'], 13))
                ->select('a.*', 'd.kode as kode_poli', 'd.uraian as nama_poli', 'e.no_bpjs', 'b.no_kunjungan')
                ->get();

            if (count($queryoperasipasiens) > 0) {
                foreach ($queryoperasipasiens as $key => $queryoperasipasien) {

                    $status = 0;
                    if ($queryoperasipasien->status == 'Menunggu') {
                        $status = 0;
                    } else {
                        $status = 1;
                    }
                    $data_array[] = array(
                        'kodebooking' => $queryoperasipasien->no_booking,
                        'tanggaloperasi' => $queryoperasipasien->tgl,
                        'jenistindakan' => $queryoperasipasien->catatan,
                        'kodepoli' => $queryoperasipasien->kode_poli,
                        'namapoli' => $queryoperasipasien->nama_poli,
                        'terlaksana' => $status,
                    );
                }
                $response = array(
                    'response' => array(
                        'list' => (
                            $data_array
                        ),
                    ),
                    'metadata' => array(
                        'message' => 'Ok',
                        'code' => 200,
                    ),
                );
                http_response_code(200);
            } else {
                $response = array(
                    'metadata' => array(
                        'message' => 'Maaf anda tidak memiliki jadwal operasi',
                        'code' => 201,
                    ),
                );
                http_response_code(201);
            }
        }
        return $response;
    }
}
