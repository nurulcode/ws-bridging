<?php

namespace App\Http\Controllers\JknV2\Antrean;

use App\Http\Controllers\JknV2\Antrean\Controller;
use App\Models\Kunjungan;
use App\Models\RefMobileJkn;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BatalnAntreanController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $decode = $request->all();
        if (empty($decode['kodebooking'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Kode Booking tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['kodebooking'], "'") || strpos($decode['kodebooking'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Kode Booking salah',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['keterangan'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Keterangan tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (strpos($decode['keterangan'], "'") || strpos($decode['keterangan'], "\\")) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Keterangan salah',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else {
            $booking = RefMobileJkn::where('no_booking', validTeks4($decode['kodebooking'], 25))->first();

            if (empty($booking->status)) {
                $response = array(
                    'metadata' => array(
                        'message' => 'Data Booking tidak ditemukan',
                        'code' => 201,
                    ),
                );
                http_response_code(201);
            } else {
                if ($booking->status == 'Batal') {
                    $response = array(
                        'metadata' => array(
                            'message' => 'Booking Anda Sudah Dibatalkan pada tanggal ' . $booking->validasi,
                            'code' => 201,
                        ),
                    );
                    http_response_code(201);
                } else if ($this->date > $booking->tgl_periksa) {
                    $response = array(
                        'metadata' => array(
                            'message' => 'Pembatalan Antrean tidak berlaku mundur',
                            'code' => 201,
                        ),
                    );
                    http_response_code(201);
                } else if ($booking->status == 'CheckIn') {
                    $response = array(
                        'metadata' => array(
                            'message' => 'Anda Sudah CheckIn Pada Tanggal ' . $booking->validasi . ', Pendaftaran Tidak Bisa Dibatalkan',
                            'code' => 201,
                        ),
                    );
                    http_response_code(201);
                } else if ($booking->status == 'Belum') {

                    try {
                        DB::beginTransaction();
                        RefMobileJkn::where('no_booking', validTeks4($decode['kodebooking'], 25))->update(['status' => 'Batal', 'validasi' => $this->date_time]);

                        $batal = Kunjungan::where('id', $booking->kunjungan_id)->delete();

                        if (!empty($batal)) {
                            $response = array(
                                'metadata' => array(
                                    'message' => 'Ok',
                                    'code' => 200,
                                ),
                            );

                            DB::table('ref_mobile_jkn_batals')->insert([
                                'pasien_id' => $booking->pasien_id,
                                'ref_mobile_jkn_id' => $booking->id,
                                'no_rekam_medis' => $booking->no_rekam_medis,
                                'no_referensi' => $booking->no_referensi,
                                'tgl_batal' => $this->date_time,
                                'keterangan' => validTeks4($decode['keterangan'], 50),
                                'status_kirim' => 'Belum',
                            ]);

                            http_response_code(200);
                        } else {
                            $response = array(
                                'metadata' => array(
                                    'message' => "Maaf Terjadi Kesalahan, Hubungi Admnistrator..",
                                    'code' => 201,
                                ),
                            );
                            http_response_code(201);
                        }
                        DB::commit();
                    } catch (\Exception$e) {
                        DB::rollback();
                        return $response = array(
                            'metadata' => array(
                                'message' => "Maaf Terjadi Kesalahan, Hubungi Admnistrator..",
                                'code' => 201,
                            ),
                        );
                        http_response_code(201);
                    }
                }
            }
        }
        return $response;
    }
}
