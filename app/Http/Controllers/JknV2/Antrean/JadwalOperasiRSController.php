<?php

namespace App\Http\Controllers\JknV2\Antrean;

use App\Http\Controllers\JknV2\Antrean\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JadwalOperasiRSController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $decode = $request->all();

        if (empty($decode['tanggalawal'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Tanggal Awal tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $decode['tanggalawal'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Tanggal Awal tidak sesuai, format yang benar adalah yyyy-mm-dd',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if ($this->date > $decode['tanggalawal']) {
            $response = array(
                'metadata' => array(
                    'message' => 'Tanggal Awal tidak berlaku mundur',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (empty($decode['tanggalakhir'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Tanggal Akhir tidak boleh kosong',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $decode['tanggalakhir'])) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format Tanggal Akhir tidak sesuai, format yang benar adalah yyyy-mm-dd',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if ($this->date > $decode['tanggalakhir']) {
            $response = array(
                'metadata' => array(
                    'message' => 'Tanggal Akhir tidak berlaku mundur',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else if ($decode['tanggalawal'] > $decode['tanggalakhir']) {
            $response = array(
                'metadata' => array(
                    'message' => 'Format tanggal awal harus lebih kecil dari tanggal akhir',
                    'code' => 201,
                ),
            );
            http_response_code(201);
        } else {

            $queryoperasirs = DB::table('booking_operasis as a')
                ->join('kunjungans as b', 'b.id', '=', 'a.kunjungan_id')
                ->join('pasien_polis as c', 'c.kunjungan_id', '=', 'b.id')
                ->join('polis as d', 'd.id', '=', 'c.poli_id')
                ->join('pasiens as e', 'e.id', '=', 'b.pasien_id')
                ->whereBetween('a.tgl', array($decode['tanggalawal'], $decode['tanggalakhir']))
                ->select('a.*', 'd.kode as kode_poli', 'd.uraian as nama_poli', 'e.no_bpjs', 'b.no_kunjungan')
                ->get();

            if (count($queryoperasirs) > 0) {
                foreach ($queryoperasirs as $key => $rsqueryoperasirs) {
                    $status = 0;
                    if ($rsqueryoperasirs->status == 'Menunggu') {
                        $status = 0;
                    } else {
                        $status = 1;
                    }
                    $data_array[] = array(
                        'kodebooking' => $rsqueryoperasirs->no_booking,
                        'tanggaloperasi' => $rsqueryoperasirs->tgl,
                        'jenistindakan' => $rsqueryoperasirs->catatan,
                        'kodepoli' => $rsqueryoperasirs->kode_poli,
                        'namapoli' => $rsqueryoperasirs->nama_poli,
                        'terlaksana' => $status,
                        'nopeserta' => $rsqueryoperasirs->no_bpjs,
                        'lastupdate' => strtotime($this->date_time) * 1000,
                    );
                }

                $response = array(
                    'response' => array(
                        'list' => (
                            $data_array
                        ),
                    ),
                    'metadata' => array(
                        'message' => 'Ok',
                        'code' => 200,
                    ),
                );
                http_response_code(200);
            } else {
                $response = array(
                    'metadata' => array(
                        'message' => 'Maaf tidak ada Jadwal Operasi pada tanggal tersebut',
                        'code' => 201,
                    ),
                );
                http_response_code(201);
            }
        }
        return $response;

    }
}
