<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // tokan antrean bpjs
        if (!empty(request()->headers->get('x-token'))) {
            request()->headers->set('Authorization', 'Bearer ' . request()->headers->get('x-token'));
        }

        // request()->request->add(['username' => request()->headers->get('x-username')]);
        // request()->request->add(['password' => request()->headers->get('x-password')]);
    }
}
