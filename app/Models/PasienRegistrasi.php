<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PasienRegistrasi extends Model
{
    use HasFactory;

    protected $table = 'pasien_registrasis';

    protected $guarded = [];
}
