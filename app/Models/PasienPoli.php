<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PasienPoli extends Model
{
    use HasFactory;

    protected $table = 'pasien_polis';

    protected $guarded = [];
}
