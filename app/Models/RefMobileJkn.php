<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RefMobileJkn extends Model
{
    use HasFactory;

    protected $table = 'ref_mobile_jkns';

    protected $guarded = [];
}
