<?php

// use Illuminate\Http\Request;
function FormatTgl($format, $tanggal)
{
    return date($format, strtotime($tanggal));
}

function hariindo($x)
{
    $hari = FormatTgl("D", $x);
    switch ($hari) {
        case 'Sun':
            $hari_ini = "Minggu";
            break;
        case 'Mon':
            $hari_ini = "Senin";
            break;
        case 'Tue':
            $hari_ini = "Selasa";
            break;
        case 'Wed':
            $hari_ini = "Rabu";
            break;
        case 'Thu':
            $hari_ini = "Kamis";
            break;
        case 'Fri':
            $hari_ini = "Jumat";
            break;
        case 'Sat':
            $hari_ini = "Sabtu";
            break;
        default:
            $hari_ini = "Tidak di ketahui";
            break;
    }

    return $hari_ini;
}

function validTeks($data)
{
    $save = str_replace("'", "", $data);
    $save = str_replace("\\", "", $save);
    $save = str_replace(";", "", $save);
    $save = str_replace("`", "", $save);
    $save = str_replace("--", "", $save);
    $save = str_replace("/*", "", $save);
    $save = str_replace("*/", "", $save);
    $save = str_replace("text/html", "", $save);
    $save = str_replace("<script>", "", $save);
    $save = str_replace("</script>", "", $save);
    $save = str_replace("<noscript>", "", $save);
    $save = str_replace("</noscript>", "", $save);
    $save = str_replace("<img", "", $save);
    $save = str_replace("document", "", $save);
    $save = str_replace(" from ", "", $save);
    $save = str_replace("concat", "", $save);
    $save = str_replace("union", "", $save);
    $save = str_replace("base64", "", $save);
    $save = str_replace("//", "", $save);
    $save = str_replace("*", "", $save);
    $save = str_replace("}", "", $save);
    $save = str_replace("$", "", $save);
    $save = str_replace("{", "", $save);
    $save = str_replace("@", "", $save);
    $save = str_replace("[", "", $save);
    $save = str_replace("]", "", $save);
    $save = str_replace("(", "", $save);
    $save = str_replace(")", "", $save);
    $save = str_replace("|", "", $save);
    $save = str_replace(",", "", $save);
    $save = str_replace("<", "", $save);
    $save = str_replace(">", "", $save);
    $save = str_replace(":", "", $save);
    $save = str_replace("+", "", $save);
    $save = str_replace("^", "", $save);
    $save = str_replace("#", "", $save);
    $save = str_replace("!", "", $save);
    $save = str_replace("='", "", $save);
    $save = str_replace("=/", "", $save);
    $save = str_replace("=", "", $save);
    $save = str_replace("=", "", $save);
    return $save;
}

function validTeks2($data)
{
    $save = str_replace("'", "", $data);
    $save = str_replace("\\", "", $save);
    $save = str_replace(";", "", $save);
    $save = str_replace("`", "", $save);
    $save = str_replace("--", "", $save);
    $save = str_replace("/*", "", $save);
    $save = str_replace("*/", "", $save);
    $save = str_replace("text/html", "", $save);
    $save = str_replace("<script>", "", $save);
    $save = str_replace("</script>", "", $save);
    $save = str_replace("<noscript>", "", $save);
    $save = str_replace("</noscript>", "", $save);
    $save = str_replace("<img", "", $save);
    $save = str_replace("document", "", $save);
    $save = str_replace(" from ", "", $save);
    $save = str_replace("concat", "", $save);
    $save = str_replace("union", "", $save);
    $save = str_replace("base64", "", $save);
    $save = str_replace("//", "", $save);
    $save = str_replace("*", "", $save);
    $save = str_replace("}", "", $save);
    $save = str_replace("$", "", $save);
    $save = str_replace("{", "", $save);
    $save = str_replace("@", "", $save);
    $save = str_replace("[", "", $save);
    $save = str_replace("]", "", $save);
    $save = str_replace("(", "", $save);
    $save = str_replace(")", "", $save);
    $save = str_replace("|", "", $save);
    $save = str_replace(",", "", $save);
    $save = str_replace("<", "", $save);
    $save = str_replace(">", "", $save);
    $save = str_replace("+", "", $save);
    $save = str_replace("^", "", $save);
    $save = str_replace("#", "", $save);
    $save = str_replace("!", "", $save);
    $save = str_replace("='", "", $save);
    $save = str_replace("=/", "", $save);
    $save = str_replace("=", "", $save);
    return $save;
}

function validTeks4($data, $panjang)
{
    $save = "";
    if (strlen($data) > $panjang) {
        header('Location: https://www.google.com');
    } else {
        $save = str_replace("'", "", $data);
        $save = str_replace("\\", "", $save);
        $save = str_replace(";", "", $save);
        $save = str_replace("`", "", $save);
        $save = str_replace("--", "", $save);
        $save = str_replace("/*", "", $save);
        $save = str_replace("*/", "", $save);
        $save = str_replace("text/html", "", $save);
        $save = str_replace("<script>", "", $save);
        $save = str_replace("</script>", "", $save);
        $save = str_replace("<noscript>", "", $save);
        $save = str_replace("</noscript>", "", $save);
        $save = str_replace("<img", "", $save);
        $save = str_replace("document", "", $save);
        $save = str_replace(" from ", "", $save);
        $save = str_replace("concat", "", $save);
        $save = str_replace("union", "", $save);
        $save = str_replace("base64", "", $save);
        $save = str_replace("//", "", $save);
        $save = str_replace("*", "", $save);
        $save = str_replace("}", "", $save);
        $save = str_replace("$", "", $save);
        $save = str_replace("{", "", $save);
        $save = str_replace("@", "", $save);
        $save = str_replace("[", "", $save);
        $save = str_replace("]", "", $save);
        $save = str_replace("(", "", $save);
        $save = str_replace(")", "", $save);
        $save = str_replace("|", "", $save);
        $save = str_replace(",", "", $save);
        $save = str_replace("<", "", $save);
        $save = str_replace(">", "", $save);
        $save = str_replace("+", "", $save);
        $save = str_replace("^", "", $save);
        $save = str_replace("#", "", $save);
        $save = str_replace("!", "", $save);
        $save = str_replace("='", "", $save);
        $save = str_replace("=/", "", $save);
        $save = str_replace("=", "", $save);
        $save = str_replace(".", "", $save);
    }
    return $save;
}

function validangka($angka)
{
    if (isset($angka)) {
        if (!is_numeric($angka)) {
            return 0;
        } else {
            return $angka;
        }
    } else {
        return 0;
    }
}

function cekpasien($nik, $nopeserta, $no_rekam_medis)
{

    $data = DB::select(DB::raw("SELECT * ,
    TIMESTAMPDIFF(YEAR ,  tgl_lahir , CURDATE()) as tahun ,
    (TIMESTAMPDIFF(MONTH , tgl_lahir , CURDATE()) - ((TIMESTAMPDIFF(MONTH , tgl_lahir , CURDATE()) div 12) * 12)) as bulan ,
    TIMESTAMPDIFF(DAY , DATE_ADD(DATE_ADD(tgl_lahir , INTERVAL TIMESTAMPDIFF(YEAR , tgl_lahir , CURDATE()) YEAR) ,
    INTERVAL TIMESTAMPDIFF(MONTH , tgl_lahir , CURDATE()) - ((TIMESTAMPDIFF(MONTH , tgl_lahir , CURDATE()) div 12) * 12) MONTH) , CURDATE()) as hari
    FROM pasiens
    WHERE no_nik=:no_nik
    AND no_bpjs=:nopeserta
    AND no_rekam_medis=:no_rekam_medis"), [
        'nopeserta' => $nopeserta,
        'no_nik' => $nik,
        'no_rekam_medis' => $no_rekam_medis,
    ]);

    return $data;
}
function cekpasien2($nik, $no_rekam_medis)
{

    $data = DB::select(DB::raw("SELECT * ,
    TIMESTAMPDIFF(YEAR ,  tgl_lahir , CURDATE()) as tahun ,
    (TIMESTAMPDIFF(MONTH , tgl_lahir , CURDATE()) - ((TIMESTAMPDIFF(MONTH , tgl_lahir , CURDATE()) div 12) * 12)) as bulan ,
    TIMESTAMPDIFF(DAY , DATE_ADD(DATE_ADD(tgl_lahir , INTERVAL TIMESTAMPDIFF(YEAR , tgl_lahir , CURDATE()) YEAR) ,
    INTERVAL TIMESTAMPDIFF(MONTH , tgl_lahir , CURDATE()) - ((TIMESTAMPDIFF(MONTH , tgl_lahir , CURDATE()) div 12) * 12) MONTH) , CURDATE()) as hari
    FROM pasiens
    WHERE no_nik=:no_nik
    AND no_rekam_medis=:no_rekam_medis"), [
        'no_nik' => $nik,
        'no_rekam_medis' => $no_rekam_medis,
    ]);

    return $data;
}

function cekpasienbaru($nik, $nopeserta)
{

    $data = DB::select(DB::raw("SELECT * ,
    TIMESTAMPDIFF(YEAR ,  tgl_lahir , CURDATE()) as tahun ,
    (TIMESTAMPDIFF(MONTH , tgl_lahir , CURDATE()) - ((TIMESTAMPDIFF(MONTH , tgl_lahir , CURDATE()) div 12) * 12)) as bulan ,
    TIMESTAMPDIFF(DAY , DATE_ADD(DATE_ADD(tgl_lahir , INTERVAL TIMESTAMPDIFF(YEAR , tgl_lahir , CURDATE()) YEAR) ,
    INTERVAL TIMESTAMPDIFF(MONTH , tgl_lahir , CURDATE()) - ((TIMESTAMPDIFF(MONTH , tgl_lahir , CURDATE()) div 12) * 12) MONTH) , CURDATE()) as hari
    FROM pasiens
    WHERE no_nik=:no_nik
    AND no_bpjs=:nopeserta"), [
        'nopeserta' => $nopeserta,
        'no_nik' => $nik,
    ]);

    return $data;
}

function noRegPoli($kd_poli, $kd_dokter, $tanggal)
{
    //jika base No.Reg nomor registrasi
    $data = DB::select(DB::raw("SELECT IFNULL(MAX(CONVERT(kunjungans.no_reg,SIGNED)),0)+1 as no_reg
    FROM kunjungans
    INNER JOIN pasien_polis ON pasien_polis.kunjungan_id=kunjungans.id
    WHERE pasien_polis.poli_id=:kd_poli
    AND pasien_polis.dokter_id=:kd_dokter
    AND kunjungans.tgl_kunjungan=:tanggal"), [
        'kd_dokter' => $kd_dokter,
        'kd_poli' => $kd_poli,
        'tanggal' => $tanggal,
    ]);

    $no_reg = sprintf("%03s", (int) $data[0]->no_reg);

    return $no_reg;
}

date_default_timezone_set('Asia/Jayapura');
$month = date('Y-m');
$date = date('Y-m-d');
$time = date('H:i:s');
$date_time = date('Y-m-d H:i:s');

function tanggalIndoDgnHari($tgl)
{
    setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US');
    \Carbon\Carbon::setLocale('id');
    return \Carbon\Carbon::parse($tgl)->isoFormat('dddd, D MMMM Y');
}
