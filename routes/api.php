<?php

use App\Http\Controllers\JknV2\Antrean\AmbilAntreanController;
use App\Http\Controllers\JknV2\Antrean\BatalnAntreanController;
use App\Http\Controllers\JknV2\Antrean\CheckInAntreanController;
use App\Http\Controllers\JknV2\Antrean\GetTokenController;
use App\Http\Controllers\JknV2\Antrean\JadwalOperasiPasienController;
use App\Http\Controllers\JknV2\Antrean\JadwalOperasiRSController;
use App\Http\Controllers\JknV2\Antrean\PasienBaruController;
use App\Http\Controllers\JknV2\Antrean\SisaAntreanController;
use App\Http\Controllers\JknV2\Antrean\StatusAntreanController;
use App\Http\Controllers\JknV2\Antrean\WSBpjs\AntreanController;
use App\Http\Controllers\JknV2\Antrean\WSBpjs\DashboardAntreanController;
use App\Http\Controllers\JknV2\Antrean\WSBpjs\JadwalDokterHfisController;
use App\Http\Controllers\JknV2\Antrean\WSBpjs\ReferensiAntrolController;
use App\Http\Controllers\JknV2\Vclaim\LembarPengajuanKlaimController;
use App\Http\Controllers\JknV2\Vclaim\MonitoringController;
use App\Http\Controllers\JknV2\Vclaim\PesertaController;
use App\Http\Controllers\JknV2\Vclaim\PRBController;
use App\Http\Controllers\JknV2\Vclaim\ReferensiController;
use App\Http\Controllers\JknV2\Vclaim\RencanaKontrolController;
use App\Http\Controllers\JknV2\Vclaim\RujukanController;
use App\Http\Controllers\JknV2\Vclaim\SepController;
use App\Http\Controllers\MobileApp\BookingController;
use App\Http\Controllers\MobileApp\DaftarJknController;
use App\Http\Controllers\MobileApp\DaftarNonJknController;
use App\Http\Controllers\MobileApp\DokterController;
use App\Http\Controllers\MobileApp\JadwalDokterController;
use App\Http\Controllers\MobileApp\KetersediaanTempatTidurController;
use App\Http\Controllers\MobileApp\KuotaPoliController;
use App\Http\Controllers\MobileApp\LoginMobileAppController;
use App\Http\Controllers\MobileApp\MobileCheckInAntreanController;
use App\Http\Controllers\MobileApp\MonitoringAntreanController;
use App\Http\Controllers\MobileApp\PoliController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::group(['middleware' => ['api', 'tokenjwt'], 'prefix' => 'auth'], function ($router) {
//     Route::get('/profile', [AuthController::class, 'profile'])->name('jknv2.fkrtl.profile');
// });

Route::group(['prefix' => 'fkrtl'], function ($router) {
    Route::get('/token', GetTokenController::class)->name('jknv2.fkrtl.token');
    Route::post('/pasienbaru', PasienBaruController::class)->name('jknv2.fkrtl.pasienbaru');
});

Route::group(['middleware' => ['api', 'tokenjwt', 'cors']], function ($router) {
    Route::group(['prefix' => 'fkrtl'], function ($router) {
        Route::post('/ambilantrean', AmbilAntreanController::class)->name('jknv2.fkrtl.antrean.ambil');
        Route::post('/statusantrean', StatusAntreanController::class)->name('jknv2.fkrtl.antrean.status');
        Route::post('/sisaantrean', SisaAntreanController::class)->name('jknv2.fkrtl.antrean.sisa');
        Route::post('/batalantrean', BatalnAntreanController::class)->name('jknv2.fkrtl.antrean.batal');
        Route::post('/checkinantrean', CheckInAntreanController::class)->name('jknv2.fkrtl.antrean.checkin');
    });
    Route::group(['prefix' => 'fkrtl'], function ($router) {
        Route::post('/jadwaloperasirs', JadwalOperasiRSController::class)->name('jknv2.fkrtl.jadwaloperasi.rs');
        Route::post('/jadwaloperasipasien', JadwalOperasiPasienController::class)->name('jknv2.fkrtl.jadwaloperasi.pasien');
    });
});

Route::prefix('jkn')->group(function () {
    Route::prefix('v2')->group(function () {

        Route::group(['middleware' => ['secret.token', 'cors']], function () {
            Route::prefix('referensi')->group(function () {

                Route::get('/diagnosa/{uraian}', [ReferensiController::class, 'getDiagnosa'])
                    ->name('jknv2.diagnosa');
                Route::get('/poli/{uraian}', [ReferensiController::class, 'getPoli'])
                    ->name('jknv2.poli');
                Route::get('/faskes/{faskes}/{jenisFaskes}', [ReferensiController::class, 'getFaskes'])
                    ->name('jknv2.faskes');
                Route::get('/dokter/pelayanan/{jenis}/tglPelayanan/{tgl}/Spesialis/{kode}', [ReferensiController::class, 'getDokterLayanan'])
                    ->name('jknv2.dokter_layanan');
                Route::get('/propinsi', [ReferensiController::class, 'getProvinsi'])
                    ->name('jknv2.propinsi');
                Route::get('/kabupaten/propinsi/{kodeProvinsi}', [ReferensiController::class, 'getKabupaten'])
                    ->name('jknv2.kabupaten');
                Route::get('/kecamatan/kabupaten/{kodeKabupaten}', [ReferensiController::class, 'getKecamatan'])
                    ->name('jknv2.kecamatan');
                Route::get('/diagnosaprb', [ReferensiController::class, 'getDiagnosaPRB'])
                    ->name('jknv2.diagnosaprb');
                Route::get('/obatprb/{namaObat}', [ReferensiController::class, 'getObatPRB'])
                    ->name('jknv2.obatprb');
                Route::get('/procedure/{uraian}', [ReferensiController::class, 'getProcedure'])
                    ->name('jknv2.procedure');
                Route::get('/kelasrawat', [ReferensiController::class, 'getKelasRawat'])
                    ->name('jknv2.kelas_rawat');
                Route::get('/dokter/{nama}', [ReferensiController::class, 'getDokter'])
                    ->name('jknv2.dokter');
                Route::get('/spesialistik', [ReferensiController::class, 'getSpesialistik'])
                    ->name('jknv2.spesialistik');
                Route::get('/ruangrawat', [ReferensiController::class, 'getRuangRawat'])
                    ->name('jknv2.ruang_rawat');
                Route::get('/carakeluar', [ReferensiController::class, 'getCaraKeluar'])
                    ->name('jknv2.cara_keluar');
                Route::get('/pascapulang', [ReferensiController::class, 'getPascaPulang'])
                    ->name('jknv2.pasca_pulang');
            });

            Route::get('/Monitoring/Kunjungan/Tanggal/{tgl}/JnsPelayanan/{jenisPelayanan}', [MonitoringController::class, 'getDataKunjungan'])
                ->name('jknv2.monitoring.data_kunjungan');
            Route::get('/Monitoring/Klaim/Tanggal/{tgl}/JnsPelayanan/{jenisPelayanan}/Status/{statusKlaim}', [MonitoringController::class, 'getDataKlaim'])
                ->name('jknv2.monitoring.data_klaim');
            Route::get('/monitoring/JasaRaharja/JnsPelayanan/{jenisPelayanan}/tglMulai/{tglMulai}/tglAkhir/{tglAkhir}', [MonitoringController::class, 'getDataKlaimJaminanJasaRaharja'])
                ->name('jknv2.monitoring.data_klaim_jaminan_jasa_raharja');
            Route::get('/monitoring/HistoriPelayanan/NoKartu/{noKartu}/tglMulai/{tglMulai}/tglAkhir/{tglAkhir}', [MonitoringController::class, 'getHistoriPelayananPeserta'])
                ->name('jknv2.monitoring.histori_pelayanan_peserta');

            // PESERTA
            Route::get('/Peserta/nokartu/{noKartu}/tglSEP/{tglPalayanan}', [PesertaController::class, 'getPesertaByNoKartu'])
                ->name('jknv2.monitoring.peserta_noka');
            Route::get('/Peserta/nik/{nik}/tglSEP/{tglPalayanan}', [PesertaController::class, 'getPesertaByNIK'])
                ->name('jknv2.monitoring.peserta_nik');

            // LPK
            Route::post('/LPK/insert', [LembarPengajuanKlaimController::class, 'postLPK'])
                ->name('jknv2.lpk.insert');
            Route::put('/LPK/update', [LembarPengajuanKlaimController::class, 'updateLPK'])
                ->name('jknv2.lpk.update');
            Route::delete('/LPK/delete', [LembarPengajuanKlaimController::class, 'deleteLPK'])
                ->name('jknv2.lpk.delete');
            Route::get('LPK/TglMasuk/{TglMasuk}/JnsPelayanan/{JnsPelayanan}', [LembarPengajuanKlaimController::class, 'getListLPK'])
                ->name('jknv2.lpk.get_list_lpk');

            // PRB
            Route::post('/PRB/insert', [PRBController::class, 'insertPRB'])
                ->name('jknv2.prb.insert');
            Route::put('/PRB/Update', [PRBController::class, 'updatePRB'])
                ->name('jknv2.prb.update');
            Route::delete('/PRB/Delete', [PRBController::class, 'deletePRB'])
                ->name('jknv2.prb.delete');
            Route::get('/prb/{noSurat}/nosep/{sep}', [PRBController::class, 'getNomorSRB'])
                ->name('jknv2.prb.surat_rujuk_balik');
            Route::get('/prb/tglMulai/{tglMulai}/tglAkhir/{tglAkhir}', [PRBController::class, 'getTanggalSRB'])
                ->name('jknv2.prb.surat_rujuk_balik_by_tanggal');

            // RENCANA KONTROL
            Route::post('/RencanaKontrol/InsertSPRI', [RencanaKontrolController::class, 'insertSPRI'])
                ->name('jknv2.rencana_kontrol.insert_spri');
            Route::put('/RencanaKontrol/UpdateSPRI', [RencanaKontrolController::class, 'updateSPRI'])
                ->name('jknv2.rencana_kontrol.update_spri');

            Route::post('/RencanaKontrol/insert', [RencanaKontrolController::class, 'insertRencanaKontrol'])
                ->name('jknv2.rencana_kontrol.insert');
            Route::put('/RencanaKontrol/Update', [RencanaKontrolController::class, 'updateRencanaKontrol'])
                ->name('jknv2.rencana_kontrol.update');
            Route::delete('/RencanaKontrol/Delete', [RencanaKontrolController::class, 'deleteRencanaKontrol'])
                ->name('jknv2.rencana_kontrol.delete');

            Route::get('/RencanaKontrol/noSuratKontrol/{noSuratKontrol}', [RencanaKontrolController::class, 'getSuratKontrolByNoSurat'])
                ->name('jknv2.rencana_kontrol.by_nomor_surat');
            Route::get('/RencanaKontrol/nosep/{noSep}', [RencanaKontrolController::class, 'getSearchSEP'])
                ->name('jknv2.rencana_kontrol.search_sep');
            Route::get('/RencanaKontrol/ListRencanaKontrol/Bulan/{bulan}/Tahun/{tahun}/Nokartu/{noKartu}/filter/{tgl}', [RencanaKontrolController::class, 'getListRencanaKontrolByNoKartu'])
                ->name('jknv2.rencana_kontrol.by_nomor_kartu');
            Route::get('/RencanaKontrol/ListRencanaKontrol/tglAwal/{tglAwal}/tglAkhir/{tglAkhir}/filter/{tgl}', [RencanaKontrolController::class, 'getListRencanaKontrol'])
                ->name('jknv2.rencana_kontrol.list');
            Route::get('/RencanaKontrol/JadwalPraktekDokter/JnsKontrol/{JnsKontrol}/KdPoli/{KdPoli}/TglRencanaKontrol/{TglRencanaKontrol}', [RencanaKontrolController::class, 'getJadwalPraktekDokterRencanaKontrol'])
                ->name('jknv2.rencana_kontrol.jadwal_dokter');
            Route::get('/RencanaKontrol/ListSpesialistik/JnsKontrol/{jnsKontrol}/nomor/{nomor}/TglRencanaKontrol/{TglRencanaKontrol}', [RencanaKontrolController::class, 'getListSpesialistikRencanaKontrol'])
                ->name('jknv2.rencana_kontrol.spesialistik');

            Route::get('/SEP/{noSep}', [SepController::class, 'getSep'])
                ->name('jknv2.sep.cari');
            Route::post('/SEP/2.0/insert', [SepController::class, 'postSep'])
                ->name('jknv2.sep.insert');
            Route::put('/SEP/2.0/update', [SepController::class, 'updateSep'])
                ->name('jknv2.sep.update');
            Route::delete('/SEP/2.0/delete', [SepController::class, 'deleteSep'])
                ->name('jknv2.sep.delete');
            Route::get('/sep/JasaRaharja/Suplesi/{noKartu}/tglPelayanan/{tglPelayanan}', [SepController::class, 'getSEPJasaRaharja'])
                ->name('jknv2.sep.get_sep_jasa_raharja');
            Route::get('/sep/KllInduk/List/{noKartu}', [SepController::class, 'getSEPKecelakaanLaluLintas'])
                ->name('jknv2.sep.get_sep_kll');
            Route::post('/Sep/pengajuanSEP', [SepController::class, 'postPengajuanSEP'])
                ->name('jknv2.sep.pengajuan_sep');
            Route::post('/Sep/aprovalSEP', [SepController::class, 'postAprovalPengajuanSEP'])
                ->name('jknv2.sep.aprove_pengajuan_sep');
            Route::put('/SEP/2.0/updtglplg', [SepController::class, 'updateTanggalPulang'])
                ->name('jknv2.sep.tanggal_pulang');
            Route::get('/Sep/updtglplg/list/bulan/{bulan}/tahun/{tahun}/{filter?}', [SepController::class, 'getListDataTanggalPulang'])
                ->name('jknv2.sep.list_tgl_pulang');
            Route::get('/SEP/Internal/{noSEP}', [SepController::class, 'getSEPInternal'])
                ->name('jknv2.sep.get_internal');
            Route::delete('/SEP/Internal/delete', [SepController::class, 'deleteSEPInternal'])
                ->name('jknv2.sep.delete_internal');
            Route::get('/SEP/FingerPrint/Peserta/{noKartu}/TglPelayanan/{tglPelayanan}', [SepController::class, 'getFingerPrint'])
                ->name('jknv2.sep.get_finger_print');
            Route::get('/SEP/FingerPrint/List/Peserta/TglPelayanan/{TglPelayanan}', [SepController::class, 'getListFingerPrint'])
                ->name('jknv2.sep.list_finger_print');

            Route::post('/Rujukan/2.0/insert', [RujukanController::class, 'postRujukan'])
                ->name('jknv2.rujukan.post');
            Route::put('/Rujukan/2.0/Update', [RujukanController::class, 'updateRujukan'])
                ->name('jknv2.rujukan.update');
            Route::delete('/Rujukan/delete', [RujukanController::class, 'deleteRujukan'])
                ->name('jknv2.rujukan.delete');
            Route::get('/Rujukan/{noRujukan}', [RujukanController::class, 'getRujukanNoRujukanByPCare'])
                ->name('jknv2.rujukan.pcare_by_no_rujukan');
            Route::get('/Rujukan/RS/{noRujukan}', [RujukanController::class, 'getRujukanNoRujukanByRumahSakit'])
                ->name('jknv2.rujukan.rs_by_no_rujukan');
            Route::get('/Rujukan/Peserta/{noKartu}', [RujukanController::class, 'getRujukanNoKartuByPCareOne'])
                ->name('jknv2.rujukan.pcare_by_no_noka');
            Route::get('/Rujukan/RS/Peserta/{noKartu}', [RujukanController::class, 'getRujukanNoKartuByRumahSakitOne'])
                ->name('jknv2.rujukan.rs_by_no_noka');
            Route::get('/Rujukan/RS/List/Peserta/{noKartu}', [RujukanController::class, 'getRujukanNoKartuByRumahSakitMulti'])
                ->name('jknv2.rujukan.rs.by_noka_multi');
            Route::get('/Rujukan/List/Peserta/{noKartu}', [RujukanController::class, 'getRujukanNoKartuByPCareMulti'])
                ->name('jknv2.rujukan.pcare.by_noka_multi');
            Route::get('/Rujukan/JumlahSEP/{jenisRujukan}/{noRujukan}', [RujukanController::class, 'getDataJumlahSEPRujukan'])
                ->name('jknv2.rujukan.data_jumlah_sep_rujukan');
            Route::get('/Rujukan/Keluar/{noRujukan}', [RujukanController::class, 'getRujukanKeluar'])
                ->name('jknv2.rujukan.rujukan_keluar');
            Route::get('/Rujukan/Keluar/List/tglMulai/{tglMulai}/tglAkhir/{tglAkhir}', [RujukanController::class, 'getListRujukanKeluarRS'])
                ->name('jknv2.rujukan.list_rujukan_keluar');
            Route::get('/Rujukan/ListSarana/PPKRujukan/{PPKRujukan}', [RujukanController::class, 'getListSarana'])
                ->name('jknv2.rujukan.list_sarana');
            Route::get('/Rujukan/ListSpesialistik/PPKRujukan/{PPKRujukan}/TglRujukan/{TglRujukan}', [RujukanController::class, 'getDataListSpesialistik'])
                ->name('jknv2.rujukan.list_spesialistik');

            Route::post('/Rujukan/Khusus/insert', [RujukanController::class, 'postRujukanKhusus'])
                ->name('jknv2.rujukan_khusus.post');
            Route::delete('/Rujukan/Khusus/delete', [RujukanController::class, 'deleteRujukanKhusus'])
                ->name('jknv2.rujukan_khusus.delete');

        });
    });
});

Route::prefix('antrean')->group(function () {
    Route::group(['middleware' => ['secret.token', 'cors']], function () {

        Route::get('/ref/poli', [ReferensiAntrolController::class, 'getPoli'])
            ->name('antrean.poli');
        Route::get('/ref/dokter', [ReferensiAntrolController::class, 'getDokter'])
            ->name('antrean.dokter');
        Route::get('/jadwaldokter/kodepoli/{poli}/tanggal/{tgl}', [ReferensiAntrolController::class, 'getJadwalDokter'])
            ->name('antrean.jadwal_dokter');
        Route::get('/ref/polifp', [ReferensiAntrolController::class, 'getPoliFp'])
            ->name('antrean.polifp');
        Route::get('/ref/pasienfp', [ReferensiAntrolController::class, 'getPasienFp'])
            ->name('antrean.pasienfp');

        Route::get('/dashboard/waktutunggu/tanggal/{tgl}/waktu/{waktu}', [DashboardAntreanController::class, 'dasWaktuPerTanggal'])
            ->name('antrean.das_waktu_tunggu_tanggal');
        Route::get('/dashboard/waktutunggu/bulan/{bulan}/tahun/{tahun}/waktu/{waktu}', [DashboardAntreanController::class, 'dasWaktuPerBulan'])
            ->name('antrean.das_waktu_tunggu_bulan');

        Route::post('/jadwaldokter/updatejadwaldokter', [JadwalDokterHfisController::class, 'updateJadwalDokter'])
            ->name('hfis.update_jadwal_dokter');
        Route::post('/add', [AntreanController::class, 'createAntreanPoli'])
            ->name('antrean.tambah_antrean');
        Route::post('/updatewaktu', [AntreanController::class, 'updateWaktuAntrean'])
            ->name('antrean.update_waktu_antrean');
        Route::post('/batal', [AntreanController::class, 'batalWaktuAntrean'])
            ->name('antrean.batal_antrean');
        Route::post('/getlisttask', [AntreanController::class, 'listWaktuTaskId'])
            ->name('antrean.list_task');
        // Route::post('/pendaftaran/kodebooking/{kodebooking}', [AntreanController::class, 'getAntreanKodeBookinf'])
        //     ->name('antrean.per_kodebooking');
    });
});

Route::prefix('mobileapp')->name('mobileapp.')->group(function () {
    Route::post('/login', LoginMobileAppController::class)->name('get_token');

    Route::group(['middleware' => ['secret.token', 'cors']], function () {

        Route::get('/list-booking/{norm}', [BookingController::class, 'getListBooking'])->name('booking.list');
        Route::post('/ambilantrean', DaftarJknController::class)->name('antrean.ambil');
        Route::post('/nonjkn/ambilantrean', DaftarNonJknController::class)->name('antrean.nonjkn.ambil');
        Route::post('/checkinantrean', MobileCheckInAntreanController::class)->name('antrean.checkin');

        Route::get('/jadwal-dokter/dokter', [DokterController::class, 'getDokterOnJadwalDokter'])
            ->name('dokter_from_jadwal_dokter');
        Route::get('/jadwal-dokter/poli', [PoliController::class, 'getPoliOnJadwalDokter'])
            ->name('poli_from_jadwal_dokter');
        Route::get('/jadwal/dokter/{dokter}', [JadwalDokterController::class, 'getJadwalDokterBaseOnDokter'])
            ->name('jadwal_dokter_base_on_dokter');
        Route::get('jadwal-dokter/poli/{poli}', [JadwalDokterController::class, 'getJadwalDokterBaseOnJadwalDokter'])
            ->name('jadwal_dokter_base_on_poli');

        Route::get('/sisakuota/poli/{poli}/tgl/{tgl}', [KuotaPoliController::class, 'getSisaKuotaPoli'])
            ->name('sisa_kuota_poli');

        Route::get('/ketersediaan-tempat-tidur', [KetersediaanTempatTidurController::class, 'getTempatTidur'])
            ->name('ketersediaan_tempat_tidur');

        Route::get('/monitor-antrean-poli', [MonitoringAntreanController::class, 'getAntreanPoli'])
            ->name('monitor_antrean_poli');

        Route::get('master/poli', [PoliController::class, 'getPoli'])->name('master.poli');
        Route::get('poli/tgl/{tgl}', [PoliController::class, 'getPoliVclaimOnDate'])->name('jadwal_poli');

        Route::get('master/dokter', [DokterController::class, 'getDokter'])
            ->name('master.dokter');
        Route::get('dokter/poli/{poli}/tgl/{tgl}', [DokterController::class, 'getDokterByPoli'])
            ->name('dokter_by_poli_tgl');
        Route::get('praktek/dokter/{dokter}/poli/{poli}/tgl/{tgl}', [DokterController::class, 'getDokterPoliTgl'])
            ->name('dokter_poli_tgl');
    });
});
